var studentlist = ""
window.onload = function() {
	$.ajax({
		type: "GET",
		url: "http://10.14.14.7:8888/v1/api/student/show_all_student",
		async: true,
		data: {
			"page_num": 1,
			"record": 20
		},
		success: function(data) {
			//console.log(data.data.data)
			//console.log(data.data.data.length)
			for(var i = 0; i < data.data.data.length; i++) {
				studentlist += "<tr>"
				studentlist += "<td>" + data.data.data[i].clazz.department.departmentName + "</td>"
				studentlist += "<td>" + data.data.data[i].clazz.className + "</td>"
				studentlist += "<td>" + data.data.data[i].studentNumber + "</td>"
				studentlist += "<td>" + data.data.data[i].studentName + "</td>"
				studentlist += "<td>" + data.data.data[i].studentPhone + "</td>"
				studentlist += "<td>" + "<a style='cursor:pointer'" + "id='" + data.data.data[i].studentId + "'" + "onclick='edit(this.id)'>编辑</a>" + "|" + "<a style='cursor:pointer'" + "id='" + data.data.data[i].studentId + "'" + "onclick='Delete(this.id)'>删除</a>" + "</td>"
				studentlist += "</tr>"
			}
			$("#studentList").append(studentlist)
		},
		error: function(data) {
			console.log(data)
		},
	});
}

function edit(suck) {
	URL = "createstudent.html?SN=" + suck
	window.location.href = URL
}

function Delete(suck) {
	//console.log(suck)
	$.ajax({
		type: "put",
		url: "http://10.14.14.7:8888/v1/api/student/delete_by_id",
		async: true,
		Accept: "application/json",
		data: {
			"student_id": suck
		},
		success: function(data) {
			console.log(data)
			window.location.reload()
		},
		error: function(data) {
			alert("something wrong happened!")
		},
	});
}

function find() {
	//$("#studentList").remove()
	var studentnumber1 = $("#searchStudent").val()
	var studentlistsearch = ""
	$.ajax({
		type: "GET",
		url: "http://10.14.14.7:8888/v1/api/student/select_by_studentnumber",
		async: true,
		Accept: "application/json",
		data: {
			"student_number": studentnumber1
		},
		success: function(data) {
			//console.log(data.data.data)
			//console.log(data.data.data.length)
			if(!data.data) {
				alert("查无此人")
				window.location.reload()
			} else {
				$("#studentList").empty()
				console.log(data.data)
				studentlistsearch += "<tr>"
				studentlistsearch += "<td>" + data.data.clazz.department.departmentName + "</td>"
				studentlistsearch += "<td>" + data.data.clazz.className + "</td>"
				studentlistsearch += "<td>" + data.data.studentNumber + "</td>"
				studentlistsearch += "<td>" + data.data.studentName + "</td>"
				studentlistsearch += "<td>" + data.data.studentPhone + "</td>"
				studentlistsearch += "<td>" + "<a style='cursor:pointer'>编辑</a>" + "|" + "<a style='cursor:pointer'" + "id='" + data.data.studentId + "'" + "onclick='Delete(this.id)'>删除</a>" + "</td>"
				studentlistsearch += "</tr>"
				console.log(studentlistsearch)
				$("#studentList").append(studentlistsearch)
			}
		},
		error: function(data) {
			console.log(data)
		},
	});

}