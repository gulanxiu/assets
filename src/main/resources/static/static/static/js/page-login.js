//console.log("show?")

function Login() {
	window.location.href = "user_index.html";

	var account = document.getElementById("Account").value;
	var password = document.getElementById("Password").value;

	if($("#pType").find("option:selected").text() === "学生") {
		$.ajax({
			type: "GET",
			url: "/api/v1/user/login",
			data: {
				"number": account,
				"password": password,
				"tag": 0
			},
			success: function(data) {
				console.log(data)
				if(data.code == 0) {
					//存储用户信息到cookie中
					window.location.href = "user_index.html";
				} else
					alert(data.msg);
			},
			error: function(data) {
				console.log(data);
			},
		})
	}
	if($("#pType").find("option:selected").text() === "老师") {
		$.ajax({
			type: "GET",
			url: "/api/v1/user/login",
			data: {
				"number": account,
				"password": password,
				"tag": 0
			},
			success: function(data) {
				console.log(data)
				if(data.code == 0) {
					//存储用户信息到cookie中
					window.location.href = "user_index.html";
				} else
					alert(data.msg);
			},
			error: function(data) {
				console.log(data);
			},
		})
	}
	if($("#pType").find("option:selected").text() === "管理员") {
		$.ajax({
			type: "GET",
			url: "/api/v1/user/login",
			data: {
				"number": account,
				"password": password,
				"tag": 0
			},
			success: function(data) {
				console.log(data)
				if(data.code == 0) {
					//存储用户信息到cookie中
					window.location.href = "index_manage.html";
				} else
					alert(data.msg);
			},
			error: function(data) {
				console.log(data);
			},
		})
	}
}