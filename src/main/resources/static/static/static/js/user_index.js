var page_num = 1
var record = 20
var student_id = 45

window.onload = function() {
	var borrow_list = ""
	//展示借用相关的列表
	$.ajax({
		type: "get",
		url: "http://10.14.14.7:8888/v1/api/borrow/show",
		async: true,
		data: {
			"page_num": page_num,
			"record": 50,
			"student_id": student_id
		},
		success: function(data) {
			console.log(data)
			//console.log(data.data.data.length)
			//console.log(data.data.data[0].device.department.departmentName)
			for(var i = 0; i < data.data.data.length; i++) {
				borrow_list += "<tr>"
				borrow_list += "<td>" + data.data.data[i].device.department.departmentName + "</td>"
				borrow_list += "<td>" + data.data.data[i].device.deviceId + "</td>"
				borrow_list += "<td>" + data.data.data[i].device.deviceName + "</td>"
				borrow_list += "<td>" + data.data.data[i].device.deviceModel + "</td>"
				borrow_list += "<td>" + data.data.data[i].device.devicePrice + "</td>"
				borrow_list += "<td>" + data.data.data[i].device.deviceLocation + "</td>"
				borrow_list += "<td>" + data.data.data[i].borrowStatus.borrowStatusName + "</td>"
				if(data.data.data[i].borrowStatus.borrowStatusName === "申请借用")
					borrow_list += "<td>" + "<button type='button' class='btn btn-default' " + "id='" + data.data.data[i].borrowId + "'" + "onclick='returnback(this.id)' disabled=’disabled'><i class='fa fa-paper-plane-o' style='margin-right: 10px;'></i>归还</button>" + "</td>"
				else if(data.data.data[i].borrowStatus.borrowStatusName === "借用中")
					borrow_list += "<td>" + "<button type='button' class='btn btn-default' " + "id='" + data.data.data[i].borrowId + "'" + "onclick='returnback(this.id)'><i class='fa fa-paper-plane-o' style='margin-right: 10px;'></i>归还</button>" + "</td>"
				else if(data.data.data[i].borrowStatus.borrowStatusName === "申请归还")
					borrow_list += "<td>" + "<button type='button' class='btn btn-default' " + "id='" + data.data.data[i].borrowId + "'" + "onclick='returnback(this.id)' disabled=’disabled'><i class='fa fa-paper-plane-o' style='margin-right: 10px;'></i>归还</button>" + "</td>"
				else if(data.data.data[i].borrowStatus.borrowStatusName === "可借")
					borrow_list += "<td>" + "<button type='button' class='btn btn-default' " + "id='" + data.data.data[i].borrowId + "'" + "onclick='returnback(this.id)' disabled=’disabled'><i class='fa fa-paper-plane-o' style='margin-right: 10px;'></i>归还</button>" + "</td>"				
				borrow_list += "</tr>"
			}
			//console.log(borrow_list)
			$("#borrow_list").append(borrow_list)
		}
	});
}

function returnback(borrowId) {
	var changeValue = {
		"back_Because": "on time",
		"borrow_id": borrowId,
		"is_good": 1,
		"state": 3
	}
	$.ajax({
		type: "PUT",
		url: "http://10.14.14.7:8888/v1/api/borrow/update_borrow",
		//async:true,
		contentType: "application/json",
		Accept: "application/json",
		data: JSON.stringify(changeValue),
		success: function(data) {
			console.log(data)
		},
		error: function(data) {
			alert("something wrong happened...")
		},
	});
}