package com.gulanxiu.assets.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.UpdateProvider;
import com.gulanxiu.assets.domain.Student;
import com.gulanxiu.assets.provider.StudentProvider;

public interface StudentMapper
{
	@Select("select * from student where student_id = #{id}  ")
	@Results(id ="student",value =
			{
				@Result(column="class_id",property="clazz",
						one = @One(select = "com.gulanxiu.assets.mapper.ClazzMapper.getClazzById"))
			})
	Student getStudentById(int id);
	
	
	
	

	@Insert("insert into student(student_number,student_name,student_phone"
			+ ",student_comment,class_id,state) values(#{studentNumber},#{studentName},#{studentPhone},"
			+ "#{studentComment},#{clazz.classId},#{state})")
	@Options(useGeneratedKeys=true,keyProperty="studentId",keyColumn="student_id")
	int addStudent(Student student);
	
	@UpdateProvider(type=StudentProvider.class,method = "update")
	void update(Student student);
	
	
	@Delete("delete from student where student_id = #{studentId}")
	void deleteById(int id);


	@ResultMap("student")
	@Select("select * from student where student_number = #{value}")
	Student selectByStudentNumber(String number);


	@ResultMap("student")
	@Select("select * from student  where state = #{state} order  by student_id ")
	List<Student> getAll(int state);


	@ResultMap("student")
	@Select("select * from student where student_number = #{value}")
	Student getByNumber(String number);
	

}
