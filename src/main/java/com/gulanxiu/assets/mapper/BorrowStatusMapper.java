package com.gulanxiu.assets.mapper;

import org.apache.ibatis.annotations.Select;

import com.gulanxiu.assets.domain.BorrowStatus;

public interface BorrowStatusMapper
{
	@Select("select * from borrow_status where borrow_status_id = #{value}")
	BorrowStatus getBorrowStatusById( int id);

}
