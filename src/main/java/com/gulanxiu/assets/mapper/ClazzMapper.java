package com.gulanxiu.assets.mapper;

import java.util.List;

import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.gulanxiu.assets.domain.Clazz;


public interface ClazzMapper
{
	@Results(id = "clazz",value ={
		@Result(column = "department_id",property ="department",
				one = @One(select = "com.gulanxiu.assets.mapper.DepartmentMapper.getDepartmentById"))
	})
	@Select("select *  from clazz where class_id = #{id}")
	Clazz getClazzById(@Param("id") int id);
	
	@Select("select * from clazz  where department_id = #{id}")
	@ResultMap("clazz")
	List<Clazz> getClazzByDepartment(int id);

}
