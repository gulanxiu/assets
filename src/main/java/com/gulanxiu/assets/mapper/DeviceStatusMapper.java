package com.gulanxiu.assets.mapper;

import org.apache.ibatis.annotations.Select;

import com.gulanxiu.assets.domain.DeviceStatus;

public interface DeviceStatusMapper
{
	@Select("select * from device_status where device_status_id = #{id}")
	DeviceStatus getDeviceStatusById( int id);

}
