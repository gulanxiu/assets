package com.gulanxiu.assets.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.gulanxiu.assets.domain.Department;

public interface DepartmentMapper
{
	@Select("select * from department where department_id = #{value}")
	Department getDepartmentById(int id);
	
	@Select("select * from department")
	List<Department> getAllDepartment();

	@Select("select department_id from department where department_name = #{value} ")
	Integer getIdByName(String departmentName);

}
