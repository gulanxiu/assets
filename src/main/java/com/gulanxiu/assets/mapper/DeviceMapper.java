package com.gulanxiu.assets.mapper;

import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;

import com.gulanxiu.assets.domain.BuyDevice;
import com.gulanxiu.assets.domain.Device;
import com.gulanxiu.assets.domain.RepairDevice;
import com.gulanxiu.assets.domain.ScrapDevice;
import com.gulanxiu.assets.domain.TransferDevice;
import com.gulanxiu.assets.domain.json.BuyDeviceForList;
import com.gulanxiu.assets.domain.json.RepairDeviceDetail;
import com.gulanxiu.assets.domain.json.RepairDeviceForList;
import com.gulanxiu.assets.domain.json.RepairExcel;
import com.gulanxiu.assets.domain.json.ScrapDeviceForList;
import com.gulanxiu.assets.domain.json.ScrapForAdd;
import com.gulanxiu.assets.domain.json.TransferDeviceForList;
import com.gulanxiu.assets.domain.json.TransferForAdd;
import com.gulanxiu.assets.domain.json.TransferForDevice;
import com.gulanxiu.assets.domain.json.TransferForExcel;
import com.gulanxiu.assets.provider.DeviceProvider;

public interface DeviceMapper
{
	@Results(id = "device",value={
		@Result(column="own_id",property="owner",one=
				@One(select="com.gulanxiu.assets.mapper.StudentMapper.getStudentById")),
			@Result(column="department_id",property="department"
			,one = @One(select="com.gulanxiu.assets.mapper.DepartmentMapper.getDepartmentById"))
			,
		@Result(column="device_status",property="deviceStatusEntity",
		one=@One(select="com.gulanxiu.assets.mapper.DeviceStatusMapper.getDeviceStatusById"))
	}
		)
	@Select("select * from device where device_id =  #{id}")
	Device getDeviceById(@Param("id") int id);

	@ResultMap("device")
	@Select("select * from device  "
			+ "where  device_name like concat('%',#{key},'%') "
			+ "order by device_is_borrow "
			 )
	List<Device> getAllDevice(String key);
	
	
	@Select("select device_id from device where device_number = #{value} ")
	Integer getDeviceByNumber(String number);
	
	@ResultMap("device")
	@Select("select * from device order by device_is_borrow")
	List<Device> getAllDevice1();
	
	

	@Options(useGeneratedKeys = true,keyColumn = "device_id",keyProperty = "deviceId")
	@Insert("insert into device(department_id,device_number,device_name,"
			+ "device_model,device_price,device_location,creat_date,device_warranty,device_factory,"
			+ "device_factory_number,device_use_directory,own_id) values("
			+ "#{department.departmentId},#{deviceNumber},#{deviceName},"
			+ "#{deviceModel},#{devicePrice},#{deviceLocation},now(),#{deviceWarranty},#{deviceFactory} "
			+ ",#{deviceFactoryNumber},#{deviceUseDirectory},#{owner.studentId})")
	void addDevice(Device device);

	
	@UpdateProvider(type=DeviceProvider.class,method = "update")
	int update(Device device);

	@Delete("delete from device where device_id = #{value}")
	int deleteDeviceById(int id);

	@ResultMap("device")
	@Select("select * from device where device_id = #{value} ")
	Device selectOne(int deviceId);

	@Select("select count(device_id) from device where device_id = #{value} and device_is_borrow = 0")
	int isExist(int deviceId);

	@Select("select id, name, model, duty_person duty_person_id,a.student_name duty_person_name"
			+ ", create_time, manager manager_id , b.student_name manager_name from buy_device d join "
			+ "student a on a.student_id = duty_person join student b on b.student_id = manager order by create_time desc ")
	List<BuyDeviceForList> getBuyDevice();

	@Insert("insert into buy_device values(null,#{name},#{model},#{count},#{price},"
			+ "#{direction},#{place},#{exist},#{use},#{because},now(),#{dutyPerson},#{manager})")
	void addBuyDevice(BuyDevice buyDevice);

	@Select("select id, in_department_id,ad.department_name in_department_name "
			+ ",t.device_id device_id , device_name,out_department_id,bd.department_name out_department_name,"
			+ " t.create_time create_time from transfer_device t join device on t.device_id = device.device_id join department ad on"
			+ " t.in_department_id = ad.department_id join department bd on bd.department_id = t.out_department_id order by create_time desc ")
	List<TransferDeviceForList> getTransferDevice();

	
	@Update("update device set department_id = #{inId} where device_id = #{deviceId} ")
    void UpdateDepartment(Integer deviceId,int inId);
	@Insert("insert into transfer_device values(null,#{deviceId},now(),#{transferId})")
	void addTransferDevice(Integer deviceId,int transferId);

	@Select("select id,r.device_id device_id,d.device_name device_name,person_id,s.student_name person_name,repair_time "
			+ "from repair_device r join device d on d.device_id = r.device_id join student  s on s.student_id = person_id order by repair_time desc ")
	List<RepairDeviceForList> getRepairDevices();

	@Insert("insert into repair_device values(null,#{deviceId},#{because},#{method},now()"
			+ ",#{isPerson},#{money},#{applyPerson},#{dutyPerson},#{department})")
	@Options(useGeneratedKeys = true,keyColumn = "id",keyProperty = "id")
	void addRepairDevice(RepairDevice buyDevice);

	@Select("select id, s.device_id device_id,device_name,device_price*device_count device_money"
			+ ", d.department_id department_id,d.department_name department_name ,"
			+ "s.create_time create_time from scrap_device s join device de on "
			+ "de.device_id = s.device_id join department d on d.department_id = "
			+ "s.use_department_id order by create_time")
	List<ScrapDeviceForList> getScrapDevices();

	@Insert("insert into scrap_device values(null, #{deviceId},now(), #{becauseId}"
			+ "  )")
	void addScrapDevice(int deviceId ,int becauseId );

	
	@Select("select id, dp.department_id department_id,dp.department_name department_name,"
			+ "s.student_id person_id,s.student_name person_name ,s.student_phone person_phone,"
			+ "r.money money,r.because because,r.method method,r.is_person is_person,"
			+ "d.device_id device_id,d.device_number device_number,d.device_name device_name,"
			+ "d.device_model model, d.device_price price,d.device_factory factory,"
			+ "d.creat_date device_time,r.repair_time create_time from repair_device r join "
			+ "student s on r.person_id = s.student_id join clazz c on c.class_id = s.class_id "
			+ "join department dp on dp.department_id ="
			+ "c.department_id join  device d on d.device_id = r.device_id where "
			+ "r.id = #{value} ")
	RepairDeviceDetail getRepairById(int id);

	@Options(useGeneratedKeys = true,keyColumn = "id",keyProperty = "becauseId")
	@Insert("insert into scrap_because(because,create_time) values(#{because},now()) ")
	int addScrapBecause(ScrapForAdd scarp);

	@Options(useGeneratedKeys = true,keyColumn = "id",keyProperty = "id")
	@Insert("insert into transfer_because values(null, #{inId},#{outId},#{because},now())")
	void addTransferBecause(TransferForAdd transfer);

	@Select("select device_number number , "
			+ "device_name name, "
			+ "device_model model,"
			+ " device_price price, "
			+ "device_factory factory,"
			+ "creat_date createTime "
			+ "from device "
			+ "join transfer_device t on device.device_id =  t.device_id "
			+ "where t.transfer_id = #{id}")
	List<TransferForDevice> getTransferForDevice(int id);

	@Select("select d1.department_name  inDepartment , "
			+ "d2.department_name outDepartment, "
			+ "because "
			+ "from transfer_because "
			+ "join department d1 on in_id = d1.department_id "
			+ "join department d2 on out_id = d2.department_id "
			+ "where id = #{value} ")
	TransferForExcel getTransferBecause(int id);

	@Select("select sum(device_price) "
			
			+ "from device "
			+ "join transfer_device t on device.device_id =  t.device_id "
			+ "where t.transfer_id = #{id}")
	BigDecimal getSumPrice(int id);

	@Select("select device_name , "
			+ "device_number, "
			+ "device_model, "
			+ "device_factory factory, "
			+ "device_price price , "
			+ "creat_date create_date, "
			+ "device_warranty warranty,"
			+ "money , "
			+ "method , "
			+ "because, "
			+ "department, "
			+ "is_person ,"
			+ "duty_person, "
			+ "apply_person "
			+ "from repair_device  r "
			+ " join device d on r.device_id = d.device_id  "
			+ " where id = #{value} ")
	RepairExcel getRepairExcel(int id);

	
	

}
