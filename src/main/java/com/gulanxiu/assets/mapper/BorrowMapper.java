package com.gulanxiu.assets.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.UpdateProvider;

import com.gulanxiu.assets.domain.Borrow;
import com.gulanxiu.assets.domain.BorrowForExcel;
import com.gulanxiu.assets.domain.json.BorrowForAdd;
import com.gulanxiu.assets.domain.json.BorrowForList;
import com.gulanxiu.assets.domain.json.BorrowForUpdate;
import com.gulanxiu.assets.provider.BorrowProvider;

public interface BorrowMapper
{
	@Results(id = "borrow",value = {
			@Result(column = "device_id",property ="device"
					,one = @One(select = "com.gulanxiu.assets.mapper.DeviceMapper.getDeviceById")),
			@Result(column="borrow_status",property="borrowStatus",one =
			@One(select="com.gulanxiu.assets.mapper.BorrowStatusMapper.getBorrowStatusById")),
			@Result(column="student_id",property="student", one = @One(
					select = "com.gulanxiu.assets.mapper.StudentMapper.getStudentById"))
	})
	@Select("select  * from borrow where student_id = #{student_id} order by borrow_status desc , borrow_id desc")
	List<Borrow> getBorrowListByStudnetId(@Param("student_id") int id);

	
	@UpdateProvider(type = BorrowProvider.class , method = "update")
	void update(BorrowForUpdate borrow);


	
	@Insert("insert into borrow(device_id,student_id,start_time,because_type,because,access) values(#{deviceId},"
			+ "#{userId},now(),#{becauseType},#{because},#{access})")
	@Options(useGeneratedKeys = true,keyProperty = "borrowId",keyColumn = "borrow_id")
	int addBorrow(BorrowForAdd borrow);


	@Select("select borrow_id borrow_id ,b.device_id device_id,device_name,device_model"
			+ ",s.student_id user_id,s.student_name user_name,start_time create_time,end_time from borrow b join device d on b.device_id = d.device_id join "
			+ "student s on b.student_id = s.student_id where b.borrow_status = #{arg0} "
			+ " order by start_time ")
	List<BorrowForList> getByState(int state);


	@Select("select borrow_status  from borrow where borrow_id = #{value}")
	Integer getState(int borrowId);


	@Select("select s.student_name borrow_person, "
			+ "de.department_name department,"
			+ "s.student_phone phone, "
			+ "s.student_number number, "
			+ "c.class_name clazz,"
			+ "b.start_time borrow_date ,"
			+ "d.device_name device_name,"
			+ "d.device_model device_model,"
			+ "d.device_factory device_factory,"
			+ "d.device_number device_number ,"
			+ "d.device_count count,"
			+ "b.because_type because ,"
			+ "b.access access "
			+ "from borrow b "
			+ "join device d on  d.device_id = b.device_id "
			+ "join student s on s.student_id = b.student_id "
			+ "join clazz c on c.class_id = s.class_id "
			+ "join department de on de.department_id = c.department_id "
			+ "where b.borrow_id = #{value} ")
	BorrowForExcel getBorrowForExcel(int id);


	@Select("select borrow_id borrow_id ,b.device_id device_id,device_name,device_model"
			+ ",s.student_id user_id,s.student_name user_name,start_time create_time,end_time from borrow b join device d on b.device_id = d.device_id join "
			+ "student s on b.student_id = s.student_id where b.borrow_status = 0 and b.student_id = #{value} "
			+ " order by start_time ")
	List<BorrowForList> getHistory(int id);
}
