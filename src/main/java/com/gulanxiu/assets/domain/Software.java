package com.gulanxiu.assets.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Software implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.software_id
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private Integer softwareId;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.department_id
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private Department department;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.software_number
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String softwareNumber;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.software_name
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String softwareName;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.software_params
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String softwareParams;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.software_price
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private BigDecimal softwarePrice;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.software_count
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private Integer softwareCount;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.software_version
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String softwareVersion;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.software_profession
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String softwareProfession;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.software_course
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String softwareCourse;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.creat_date
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	@JsonFormat(pattern="yyyy年MM月dd日 hh时mm分ss秒",locale="zh",timezone="GMT+8")
	private Date creatDate;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.device_factory
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String deviceFactory;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column software.device_factory_number
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String deviceFactoryNumber;

	public Integer getSoftwareId()
	{
		return softwareId;
	}

	public void setSoftwareId(Integer softwareId)
	{
		this.softwareId = softwareId;
	}

	public Department getDepartment()
	{
		return department;
	}

	public void setDepartment(Department department)
	{
		this.department = department;
	}

	public String getSoftwareNumber()
	{
		return softwareNumber;
	}

	public void setSoftwareNumber(String softwareNumber)
	{
		this.softwareNumber = softwareNumber;
	}

	public String getSoftwareName()
	{
		return softwareName;
	}

	public void setSoftwareName(String softwareName)
	{
		this.softwareName = softwareName;
	}

	public String getSoftwareParams()
	{
		return softwareParams;
	}

	public void setSoftwareParams(String softwareParams)
	{
		this.softwareParams = softwareParams;
	}

	public BigDecimal getSoftwarePrice()
	{
		return softwarePrice;
	}

	public void setSoftwarePrice(BigDecimal softwarePrice)
	{
		this.softwarePrice = softwarePrice;
	}

	public Integer getSoftwareCount()
	{
		return softwareCount;
	}

	public void setSoftwareCount(Integer softwareCount)
	{
		this.softwareCount = softwareCount;
	}

	public String getSoftwareVersion()
	{
		return softwareVersion;
	}

	public void setSoftwareVersion(String softwareVersion)
	{
		this.softwareVersion = softwareVersion;
	}

	public String getSoftwareProfession()
	{
		return softwareProfession;
	}

	public void setSoftwareProfession(String softwareProfession)
	{
		this.softwareProfession = softwareProfession;
	}

	public String getSoftwareCourse()
	{
		return softwareCourse;
	}

	public void setSoftwareCourse(String softwareCourse)
	{
		this.softwareCourse = softwareCourse;
	}

	public Date getCreatDate()
	{
		return creatDate;
	}

	public void setCreatDate(Date creatDate)
	{
		this.creatDate = creatDate;
	}

	public String getDeviceFactory()
	{
		return deviceFactory;
	}

	public void setDeviceFactory(String deviceFactory)
	{
		this.deviceFactory = deviceFactory;
	}

	public String getDeviceFactoryNumber()
	{
		return deviceFactoryNumber;
	}

	public void setDeviceFactoryNumber(String deviceFactoryNumber)
	{
		this.deviceFactoryNumber = deviceFactoryNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column software.software_id
	 *
	 * @return the value of software.software_id
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	
}