package com.gulanxiu.assets.domain;

import java.io.Serializable;

public class Perm implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column perm.perm_id
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private Integer permId;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column perm.perm_name
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String permName;

	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to the
	 * database column perm.perm_commit
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	private String permCommit;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column perm.perm_id
	 *
	 * @return the value of perm.perm_id
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	public Integer getPermId()
	{
		return permId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column perm.perm_id
	 *
	 * @param permId the value for perm.perm_id
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	public void setPermId(Integer permId)
	{
		this.permId = permId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column perm.perm_name
	 *
	 * @return the value of perm.perm_name
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	public String getPermName()
	{
		return permName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column perm.perm_name
	 *
	 * @param permName the value for perm.perm_name
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	public void setPermName(String permName)
	{
		this.permName = permName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value
	 * of the database column perm.perm_commit
	 *
	 * @return the value of perm.perm_commit
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	public String getPermCommit()
	{
		return permCommit;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of
	 * the database column perm.perm_commit
	 *
	 * @param permCommit the value for perm.perm_commit
	 *
	 * @mbg.generated Sun Oct 14 14:15:38 CST 2018
	 */
	public void setPermCommit(String permCommit)
	{
		this.permCommit = permCommit;
	}
}