package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TransferDeviceForList implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id ;
	
	@JsonProperty("in_department_id")
	private int inDepartmentId;
	
	@JsonProperty("in_department_name")
	private String inDepartmentName;
	
	@JsonProperty("out_department_id")
	private int outDepartmentId;
	
	@JsonProperty("out_department_name")
	private String outDepartmentName;
	
	@JsonProperty("device_id")
	private int deviceId;
	
	@JsonProperty("device_name")
	private String deviceName;
	
	@JsonProperty("create_time")
	@JsonFormat(pattern = "YYYY-MM-dd",locale = "zh-CN",timezone = "GMT+8")
	private Date createTime;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getInDepartmentId()
	{
		return inDepartmentId;
	}

	public void setInDepartmentId(int inDepartmentId)
	{
		this.inDepartmentId = inDepartmentId;
	}

	public String getInDepartmentName()
	{
		return inDepartmentName;
	}

	public void setInDepartmentName(String inDepartmentName)
	{
		this.inDepartmentName = inDepartmentName;
	}

	public int getOutDepartmentId()
	{
		return outDepartmentId;
	}

	public void setOutDepartmentId(int outDepartmentId)
	{
		this.outDepartmentId = outDepartmentId;
	}

	public String getOutDepartmentName()
	{
		return outDepartmentName;
	}

	public void setOutDepartmentName(String outDepartmentName)
	{
		this.outDepartmentName = outDepartmentName;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}
	
	
	
	


}
