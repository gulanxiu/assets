package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TransferForAdd implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonIgnore
	private int id;
	
	@JsonProperty("id_list")
	private List<Integer> ids;
	
	@JsonProperty("in_id")
	private int inId;
	
	@JsonProperty("out_id")
	private int outId;
	
	private String because;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public List<Integer> getIds()
	{
		return ids;
	}

	public void setIds(List<Integer> ids)
	{
		this.ids = ids;
	}

	public int getInId()
	{
		return inId;
	}

	public void setInId(int inId)
	{
		this.inId = inId;
	}

	public int getOutId()
	{
		return outId;
	}

	public void setOutId(int outId)
	{
		this.outId = outId;
	}

	public String getBecause()
	{
		return because;
	}

	public void setBecause(String because)
	{
		this.because = because;
	}
	
	

}
