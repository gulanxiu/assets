package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import java.util.List;

public class TransferForExcel implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<TransferForDevice> list;
	
	private String because;
	
	private double allPrice;
	
	private String inDepartment;
	
	private String outDepartment;
	
	

	public String getInDepartment()
	{
		return inDepartment;
	}

	public void setInDepartment(String inDepartment)
	{
		this.inDepartment = inDepartment;
	}

	public String getOutDepartment()
	{
		return outDepartment;
	}

	public void setOutDepartment(String outDepartment)
	{
		this.outDepartment = outDepartment;
	}

	public List<TransferForDevice> getList()
	{
		return list;
	}

	public void setList(List<TransferForDevice> list)
	{
		this.list = list;
	}

	public String getBecause()
	{
		return because;
	}

	public void setBecause(String because)
	{
		this.because = because;
	}

	public double getAllPrice()
	{
		return allPrice;
	}

	public void setAllPrice(double allPrice)
	{
		this.allPrice = allPrice;
	}
	
	

}
