package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BorrowForList implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("borrow_id")
	private int borrowId;
	
	@JsonFormat(pattern = "YYYY-MM-dd",locale="zh-CN",timezone="GMT+8")
	@JsonProperty("create_time")
	private Date createTime;
	
	@JsonFormat(pattern = "YYYY-MM-dd",locale="zh-CN",timezone="GMT+8")
	@JsonProperty("end_time")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Date endTime;
	
	

	@JsonProperty("device_id")
	private int deviceId;
	
	@JsonProperty("device_name")
	private String deviceName;
	
	@JsonProperty("device_model")
	private String deviceModel;
	
	@JsonProperty("user_id")
	private int userId;
	
	@JsonProperty("user_name")
	private String userName;

	public int getBorrowId()
	{
		return borrowId;
	}

	public void setBorrowId(int borrowId)
	{
		this.borrowId = borrowId;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public int getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(int deviceId)
	{
		this.deviceId = deviceId;
	}

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}

	public int getUserId()
	{
		return userId;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
	public Date getEndTime()
	{
		return endTime;
	}

	public void setEndTime(Date endTime)
	{
		this.endTime = endTime;
	}

	public String getDeviceModel()
	{
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel)
	{
		this.deviceModel = deviceModel;
	}
	

}
