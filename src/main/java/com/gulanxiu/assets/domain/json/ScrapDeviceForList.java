package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
public class ScrapDeviceForList implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	
	@JsonProperty("device_id")
	private int deviceId;
	
	@JsonProperty("device_name")
	private String deviceName;
	
	@JsonProperty("device_money")
	private BigDecimal deviceMoney;
	
	@JsonProperty("department_id")
	private int departmentId;
	
	@JsonProperty("department_name")
	private String departmentName;
	
	@JsonProperty("create_time")
	@JsonFormat(pattern="YYYY-MM-dd",locale="zh-CN",timezone="GMT+8")
	private Date createTime;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(int deviceId)
	{
		this.deviceId = deviceId;
	}

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}

	public BigDecimal getDeviceMoney()
	{
		return deviceMoney;
	}

	public void setDeviceMoney(BigDecimal deviceMoney)
	{
		this.deviceMoney = deviceMoney;
	}

	public int getDepartmentId()
	{
		return departmentId;
	}

	public void setDepartmentId(int departmentId)
	{
		this.departmentId = departmentId;
	}

	public String getDepartmentName()
	{
		return departmentName;
	}

	public void setDepartmentName(String departmentName)
	{
		this.departmentName = departmentName;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	
	
}
