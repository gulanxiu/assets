package com.gulanxiu.assets.domain.json;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RepairDeviceDetail extends DeviceBase
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("department")
	private String department;
	
	@JsonProperty("apply_person")
	private String applyPerson;
	
	@JsonProperty("duty_person")
	private String dutyPerson;
	
	private BigDecimal money;
	
	private String because;
	
	private String method;
	
	private int warranty;
	
	@JsonProperty("is_person")
	private String isPerson;
	
	

	public String getDepartment()
	{
		return department;
	}

	public void setDepartment(String department)
	{
		this.department = department;
	}

	public String getApplyPerson()
	{
		return applyPerson;
	}

	public void setApplyPerson(String applyPerson)
	{
		this.applyPerson = applyPerson;
	}

	public String getDutyPerson()
	{
		return dutyPerson;
	}

	public void setDutyPerson(String dutyPerson)
	{
		this.dutyPerson = dutyPerson;
	}

	public BigDecimal getMoney()
	{
		return money;
	}

	public void setMoney(BigDecimal money)
	{
		this.money = money;
	}

	public String getBecause()
	{
		return because;
	}

	public void setBecause(String because)
	{
		this.because = because;
	}

	public String getMethod()
	{
		return method;
	}

	public void setMethod(String method)
	{
		this.method = method;
	}

	public int getWarranty()
	{
		return warranty;
	}

	public void setWarranty(int warranty)
	{
		this.warranty = warranty;
	}

	public String getIsPerson()
	{
		return isPerson;
	}

	public void setIsPerson(String isPerson)
	{
		this.isPerson = isPerson;
	}

	
	
	
}
