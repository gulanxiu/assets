package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
public class BorrowForAdd implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	private int borrowId;
	@JsonProperty("user_id")
	private int userId;
	
	@JsonProperty("device_id")
	private int deviceId;
	
	/**
	 * 借用原因
	 */
	
	private String because;
	
	@JsonProperty("because_type")
	private String becauseType;
	
	private String access;

	
	
	public int getBorrowId()
	{
		return borrowId;
	}

	public void setBorrowId(int borrowId)
	{
		this.borrowId = borrowId;
	}

	public String getBecauseType()
	{
		return becauseType;
	}

	public void setBecauseType(String becauseType)
	{
		this.becauseType = becauseType;
	}

	public String getAccess()
	{
		return access;
	}

	public void setAccess(String access)
	{
		this.access = access;
	}

	public int getUserId()
	{
		return userId;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	public int getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(int deviceId)
	{
		this.deviceId = deviceId;
	}

	public String getBecause()
	{
		return because;
	}

	public void setBecause(String because)
	{
		this.because = because;
	}
	
	

}
