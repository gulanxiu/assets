package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ScrapForAdd implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@JsonProperty("device_id_list")
	private List<Integer> deviceIds;
	
	private String because;
	
	@JsonIgnore
	private int becauseId;
	
	


	public int getBecauseId()
	{
		return becauseId;
	}

	public void setBecauseId(int becauseId)
	{
		this.becauseId = becauseId;
	}

	public List<Integer> getDeviceIds()
	{
		return deviceIds;
	}

	public void setDeviceIds(List<Integer> deviceIds)
	{
		this.deviceIds = deviceIds;
	}

	public String getBecause()
	{
		return because;
	}

	public void setBecause(String because)
	{
		this.because = because;
	}
	
	

}
