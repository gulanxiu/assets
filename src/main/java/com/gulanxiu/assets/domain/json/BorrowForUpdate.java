package com.gulanxiu.assets.domain.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BorrowForUpdate implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("borrow_id")
	private int borrowId;
	
	/**
	 * 借用状态
	 */
	private int state;
	
	@JsonProperty("back_Because")
	private String backBecause;
	
	@JsonProperty("is_good")
	
	private int isGood;
	
	
	@JsonProperty("agree_borrow")
	private String agreeBorrow;
	
	@JsonProperty("agree_back")
	private String agreeBack;
	
	@JsonProperty("deny")
	private Integer deny;
	
	
	
	
	

	public Integer getDeny()
	{
		return deny;
	}

	public void setDeny(Integer deny)
	{
		this.deny = deny;
	}

	public String getAgreeBorrow()
	{
		return agreeBorrow;
	}

	public void setAgreeBorrow(String agreeBorrow)
	{
		this.agreeBorrow = agreeBorrow;
	}

	public String getAgreeBack()
	{
		return agreeBack;
	}

	public void setAgreeBack(String agreeBack)
	{
		this.agreeBack = agreeBack;
	}

	public int getBorrowId()
	{
		return borrowId;
	}

	public void setBorrowId(int borrowId)
	{
		this.borrowId = borrowId;
	}

	public int getState()
	{
		return state;
	}

	public void setState(int state)
	{
		this.state = state;
	}

	public String getBackBecause()
	{
		return backBecause;
	}

	public void setBackBecause(String backBecause)
	{
		this.backBecause = backBecause;
	}

	public int getIsGood()
	{
		return isGood;
	}

	public void setIsGood(int isGood)
	{
		this.isGood = isGood;
	}
	
	
	
	

}
