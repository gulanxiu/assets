package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
public class DeviceBase implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	
	@JsonProperty("device_id")
	private int deviceId;
	
	@JsonProperty("device_number")
	private String deviceNumber;
	
	@JsonProperty("device_name")
	private String deviceName;
	
	private String model;
	
	private BigDecimal price;
	
	private String factory;
	
	@JsonProperty("device_time")
	@JsonFormat(pattern = "YYYY-MM-dd",locale="zh-CN",timezone="GMT+8")
	private String deviceTime;
	
	@JsonProperty("creat_time")
	@JsonFormat(pattern = "YYYY-MM-dd",locale="zh-CN",timezone="GMT+8")
	private Date createTime;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(int deviceId)
	{
		this.deviceId = deviceId;
	}

	public String getDeviceNumber()
	{
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber)
	{
		this.deviceNumber = deviceNumber;
	}

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}

	public String getModel()
	{
		return model;
	}

	public void setModel(String model)
	{
		this.model = model;
	}

	public BigDecimal getPrice()
	{
		return price;
	}

	public void setPrice(BigDecimal price)
	{
		this.price = price;
	}

	public String getFactory()
	{
		return factory;
	}

	public void setFactory(String factory)
	{
		this.factory = factory;
	}

	public String getDeviceTime()
	{
		return deviceTime;
	}

	public void setDeviceTime(String deviceTime)
	{
		this.deviceTime = deviceTime;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	
	
}
