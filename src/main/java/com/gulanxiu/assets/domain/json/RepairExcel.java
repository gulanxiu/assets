package com.gulanxiu.assets.domain.json;

import java.math.BigDecimal;
import java.util.Date;

import com.gulanxiu.assets.domain.RepairDevice;

public class RepairExcel extends RepairDevice

{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String deviceName;
	
	private String deviceNumber;
	
	private String deviceModel;
	
	private String factory;
	
	private BigDecimal price;
	
	private Date createDate;
	
	private Integer warranty;

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}

	public String getDeviceNumber()
	{
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber)
	{
		this.deviceNumber = deviceNumber;
	}

	public String getDeviceModel()
	{
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel)
	{
		this.deviceModel = deviceModel;
	}

	public String getFactory()
	{
		return factory;
	}

	public void setFactory(String factory)
	{
		this.factory = factory;
	}

	public BigDecimal getPrice()
	{
		return price;
	}

	public void setPrice(BigDecimal price)
	{
		this.price = price;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public Integer getWarranty()
	{
		return warranty;
	}

	public void setWarranty(Integer warranty)
	{
		this.warranty = warranty;
	}
	
	

}
