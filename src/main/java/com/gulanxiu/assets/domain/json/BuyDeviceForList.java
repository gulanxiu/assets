package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BuyDeviceForList implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	
	private String name;
	
	private String model;
	
	@JsonProperty("duty_personName")
	private String dutyPersonName;
	
	@JsonProperty("duty_person_id")
	private int dutyPersonId;
	
	@JsonProperty("manager_id")
	private int managerId;
	
	@JsonProperty("manager_name")
	private String managerName;
	
	@JsonProperty("creat_time")
	@JsonFormat(pattern="YYYY-MM-dd",locale="zh-CN",timezone="GMT+8")
	private Date createTime;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getModel()
	{
		return model;
	}

	public void setModel(String model)
	{
		this.model = model;
	}

	public String getDutyPersonName()
	{
		return dutyPersonName;
	}

	public void setDutyPersonName(String dutyPersonName)
	{
		this.dutyPersonName = dutyPersonName;
	}

	public int getDutyPersonId()
	{
		return dutyPersonId;
	}

	public void setDutyPersonId(int dutyPersonId)
	{
		this.dutyPersonId = dutyPersonId;
	}

	public int getManagerId()
	{
		return managerId;
	}

	public void setManagerId(int managerId)
	{
		this.managerId = managerId;
	}

	public String getManagerName()
	{
		return managerName;
	}

	public void setManagerName(String managerName)
	{
		this.managerName = managerName;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}
	
	
	
	

}
