package com.gulanxiu.assets.domain.json;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RepairDeviceForList implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	
	@JsonProperty("device_id")
	private int deviceId;
	
	@JsonProperty("device_name")
	private String deviceName;
	
	@JsonProperty("person_id")
	private int personId;
	
	@JsonProperty("person_name")
	private String personName;
	
	@JsonProperty("repair_time")
	@JsonFormat(pattern = "YYYY-MM-dd",locale = "zh-CN",timezone = "GMT+8")
	private Date repairTime;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(int deviceId)
	{
		this.deviceId = deviceId;
	}

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}

	public int getPersonId()
	{
		return personId;
	}

	public void setPersonId(int personId)
	{
		this.personId = personId;
	}

	public String getPersonName()
	{
		return personName;
	}

	public void setPersonName(String personName)
	{
		this.personName = personName;
	}

	public Date getRepairTime()
	{
		return repairTime;
	}

	public void setRepairTime(Date repairTime)
	{
		this.repairTime = repairTime;
	}
	
	

}
