package com.gulanxiu.assets.domain;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TransferDevice implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.device_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    @JsonProperty("device_id")
    private Integer deviceId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.out_department_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    
    @JsonProperty("out_department_id")
    private Integer outDepartmentId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.in_department_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    @JsonProperty("in_department_id")
    private Integer inDepartmentId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.out_manager_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    @JsonProperty("out_manager_id")
    private Integer outManagerId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.in_manager_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    @JsonProperty("in_manager_id")
    private Integer inManagerId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.out_use_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    @JsonProperty("out_use_id")
    private Integer outUseId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.in_use_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    @JsonProperty("in_use_id")
    private Integer inUseId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.out_duty_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    @JsonProperty("out_dutyJ_id")
    private Integer outDutyId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.in_duty_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    @JsonProperty("in_duty_id")
    private Integer inDutyId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column transfer_device.create_time
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    @JsonProperty("create_time")
    @JsonFormat(pattern = "YYYY-MM-dd",locale = "zh-CN",timezone = "GMT+8")
    private Date createTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.id
     *
     * @return the value of transfer_device.id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.id
     *
     * @param id the value for transfer_device.id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.device_id
     *
     * @return the value of transfer_device.device_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getDeviceId() {
        return deviceId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.device_id
     *
     * @param deviceId the value for transfer_device.device_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.out_department_id
     *
     * @return the value of transfer_device.out_department_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getOutDepartmentId() {
        return outDepartmentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.out_department_id
     *
     * @param outDepartmentId the value for transfer_device.out_department_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setOutDepartmentId(Integer outDepartmentId) {
        this.outDepartmentId = outDepartmentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.in_department_id
     *
     * @return the value of transfer_device.in_department_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getInDepartmentId() {
        return inDepartmentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.in_department_id
     *
     * @param inDepartmentId the value for transfer_device.in_department_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setInDepartmentId(Integer inDepartmentId) {
        this.inDepartmentId = inDepartmentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.out_manager_id
     *
     * @return the value of transfer_device.out_manager_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getOutManagerId() {
        return outManagerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.out_manager_id
     *
     * @param outManagerId the value for transfer_device.out_manager_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setOutManagerId(Integer outManagerId) {
        this.outManagerId = outManagerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.in_manager_id
     *
     * @return the value of transfer_device.in_manager_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getInManagerId() {
        return inManagerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.in_manager_id
     *
     * @param inManagerId the value for transfer_device.in_manager_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setInManagerId(Integer inManagerId) {
        this.inManagerId = inManagerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.out_use_id
     *
     * @return the value of transfer_device.out_use_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getOutUseId() {
        return outUseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.out_use_id
     *
     * @param outUseId the value for transfer_device.out_use_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setOutUseId(Integer outUseId) {
        this.outUseId = outUseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.in_use_id
     *
     * @return the value of transfer_device.in_use_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getInUseId() {
        return inUseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.in_use_id
     *
     * @param inUseId the value for transfer_device.in_use_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setInUseId(Integer inUseId) {
        this.inUseId = inUseId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.out_duty_id
     *
     * @return the value of transfer_device.out_duty_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getOutDutyId() {
        return outDutyId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.out_duty_id
     *
     * @param outDutyId the value for transfer_device.out_duty_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setOutDutyId(Integer outDutyId) {
        this.outDutyId = outDutyId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.in_duty_id
     *
     * @return the value of transfer_device.in_duty_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Integer getInDutyId() {
        return inDutyId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.in_duty_id
     *
     * @param inDutyId the value for transfer_device.in_duty_id
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setInDutyId(Integer inDutyId) {
        this.inDutyId = inDutyId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column transfer_device.create_time
     *
     * @return the value of transfer_device.create_time
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column transfer_device.create_time
     *
     * @param createTime the value for transfer_device.create_time
     *
     * @mbg.generated Sun Nov 11 10:21:19 CST 2018
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}