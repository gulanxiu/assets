package com.gulanxiu.assets.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Borrow implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer borrowId;

	private Device device;

	private BorrowStatus borrowStatus;

	private Student student;

	@JsonFormat(pattern="yyyy年MM月dd日 hh时mm分ss秒",locale="zh",timezone="GMT+8")
	private Date startTime;
	
	@JsonFormat(pattern="yyyy年MM月dd日 hh时mm分ss秒",locale="zh",timezone="GMT+8")
	private Date endTime;

	private Boolean isOntime;

	private Boolean isGood;

	private String becauseType;

	private String returnBecause;
	
	private String because;
	
	private String access;
	
	private Integer deny;
	
	private String agreeBack;
	
	private String agreeBorrow;
	
	
	
	

	public Integer getDeny()
	{
		return deny;
	}

	public void setDeny(Integer deny)
	{
		this.deny = deny;
	}

	public String getAgreeBack()
	{
		return agreeBack;
	}

	public void setAgreeBack(String agreeBack)
	{
		this.agreeBack = agreeBack;
	}

	public String getAgreeBorrow()
	{
		return agreeBorrow;
	}

	public void setAgreeBorrow(String agreeBorrow)
	{
		this.agreeBorrow = agreeBorrow;
	}

	public String getAccess()
	{
		return access;
	}

	public void setAccess(String access)
	{
		this.access = access;
	}

	public Integer getBorrowId()
	{
		return borrowId;
	}

	public void setBorrowId(Integer borrowId)
	{
		this.borrowId = borrowId;
	}

	public Device getDevice()
	{
		return device;
	}

	public void setDevice(Device device)
	{
		this.device = device;
	}

	public BorrowStatus getBorrowStatus()
	{
		return borrowStatus;
	}

	public void setBorrowStatus(BorrowStatus borrowStatus)
	{
		this.borrowStatus = borrowStatus;
	}

	public Student getStudent()
	{
		return student;
	}

	public void setStudent(Student student)
	{
		this.student = student;
	}

	public Date getStartTime()
	{
		return startTime;
	}

	public void setStartTime(Date startTime)
	{
		this.startTime = startTime;
	}

	public Date getEndTime()
	{
		return endTime;
	}

	public void setEndTime(Date endTime)
	{
		this.endTime = endTime;
	}

	public Boolean getIsOntime()
	{
		return isOntime;
	}

	public void setIsOntime(Boolean isOntime)
	{
		this.isOntime = isOntime;
	}

	public Boolean getIsGood()
	{
		return isGood;
	}

	public void setIsGood(Boolean isGood)
	{
		this.isGood = isGood;
	}

	public String getBecauseType()
	{
		return becauseType;
	}

	public void setBecauseType(String becauseType)
	{
		this.becauseType = becauseType;
	}

	public String getReturnBecause()
	{
		return returnBecause;
	}

	public void setReturnBecause(String returnBecause)
	{
		this.returnBecause = returnBecause;
	}

	public String getBecause()
	{
		return because;
	}

	public void setBecause(String because)
	{
		this.because = because;
	}

	
	
}
