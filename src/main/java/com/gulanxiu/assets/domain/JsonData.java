package com.gulanxiu.assets.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;


public class JsonData implements Serializable
{ 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int code;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String msg;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Object data;
	
	public JsonData()
	{
		code = 0;
	}
	
	public JsonData(String msg)
	{
		this.msg = msg;
		code = -1;
	}
	
	public JsonData(Object data)
	{
		this.data = data;
		code = 0;
	}
	
	public static JsonData success()
	{
		return new JsonData();
	}
	
	public static JsonData success(Object data)
	{
		return new JsonData(data);
	}
	
	public static JsonData fail(String msg)
	{
		return new JsonData(msg);
	}

	public int getCode()
	{
		return code;
	}

	public void setCode(int code)
	{
		this.code = code;
	}

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

	public Object getData()
	{
		return data;
	}

	public void setData(Object data)
	{
		this.data = data;
	}

	
}
