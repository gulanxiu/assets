package com.gulanxiu.assets.domain;

import java.io.Serializable;
import java.sql.Date;

public class BorrowForExcel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String borrowPerson;
	
	private String department;
	
	private String phone;
	
	private String number;
	
	private String clazz;
	
	private Date borrowDate;
	
	private String deviceName;
	
	private String deviceFactory;
	
	private String deviceModel;
	
	private String deviceNumber;
	
	private int count;
	
	private String access;
	
	private String because;

	public String getBorrowPerson()
	{
		return borrowPerson;
	}

	public void setBorrowPerson(String borrowPerson)
	{
		this.borrowPerson = borrowPerson;
	}

	public String getDepartment()
	{
		return department;
	}

	public void setDepartment(String department)
	{
		this.department = department;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getNumber()
	{
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}

	public String getClazz()
	{
		return clazz;
	}

	public void setClazz(String clazz)
	{
		this.clazz = clazz;
	}

	public Date getBorrowDate()
	{
		return borrowDate;
	}

	public void setBorrowDate(Date borrowDate)
	{
		this.borrowDate = borrowDate;
	}

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceType)
	{
		deviceName = deviceType;
	}

	public String getDeviceFactory()
	{
		return deviceFactory;
	}

	public void setDeviceFactory(String deviceFactory)
	{
		this.deviceFactory = deviceFactory;
	}

	public String getDeviceModel()
	{
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel)
	{
		this.deviceModel = deviceModel;
	}

	public String getDeviceNumber()
	{
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber)
	{
		this.deviceNumber = deviceNumber;
	}

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public String getAccess()
	{
		return access;
	}

	public void setAccess(String access)
	{
		this.access = access;
	}

	public String getBecause()
	{
		return because;
	}

	public void setBecause(String because)
	{
		this.because = because;
	}
	
	

}
