package com.gulanxiu.assets;

import java.util.Properties;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.github.pagehelper.PageHelper;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan("com.gulanxiu.assets.mapper")
@EnableSwagger2
public class AssetsApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(AssetsApplication.class, args);
	}

	/**
	 * 分页插件
	 */
	@Bean
	public PageHelper pageHelper()
	{
		PageHelper pageHelper = new PageHelper();
		Properties p = new Properties();
		p.setProperty("offsetAsPageNum", "true");
		p.setProperty("rowBoundsWithCount", "true");
		p.setProperty("pageSizeZero", "false");
		pageHelper.setProperties(p);
		return pageHelper;
	}

	/**
	 * swagger插件
	 * 
	 * @return
	 */
	@Bean
	public Docket createRestApi()
	{
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				// 为当前包路径
				.apis(RequestHandlerSelectors.basePackage("com.gulanxiu.assets.controller")).paths(PathSelectors.any())
				.build();
	}

	private ApiInfo apiInfo()
	{
		return new ApiInfoBuilder()
				// 页面标题
				.title("Spring Boot 测试")
				// 创建人
				.contact(new Contact("gulanxiu", "", ""))
				// 版本号
				.version("1.0")
				// 描述
				.description("API 描述").build();
	}

	/**
	 * 跨域插件
	 * 
	 * @return
	 */
	private CorsConfiguration buildConfig()
	{
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		// 允许任何域名使用
		corsConfiguration.addAllowedOrigin("*");
		// 允许任何头
		corsConfiguration.addAllowedHeader("*");
		// 允许任何方法（post、get等）
		corsConfiguration.addAllowedMethod("*");
		return corsConfiguration;
	}

	@Bean
	public CorsFilter corsFilter()
	{
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		// 对接口配置跨域设置
		source.registerCorsConfiguration("/**", buildConfig());

		return new CorsFilter(source);
	}
}
