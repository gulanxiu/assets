package com.gulanxiu.assets.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.gulanxiu.assets.domain.JsonData;
import com.gulanxiu.assets.domain.Software;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api( description = "软件操作")
@RequestMapping("/v1/api/software")
public class SoftWareController
{
	@ApiOperation("显示所有软件")
	@GetMapping("show_all_software")
	public JsonData showAllClazz()
	{
		return JsonData.success();
	}
	
	
	@ApiOperation("增加一个软件")
	@PostMapping("add_softwate")
	public JsonData addClazz(@RequestBody Software software)
	{
		return JsonData.success();
	}
	
	@ApiOperation("修改软件信息")
	@PutMapping("update")
	public JsonData update(@RequestBody Software software)
	{
		return JsonData.success();
	}
	
	@ApiOperation("查询某个软件")
	@GetMapping("select_one")
	public JsonData selectOne(@RequestParam("software_id") int softwareId)
	{
		return JsonData.success();
	}


}
