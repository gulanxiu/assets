package com.gulanxiu.assets.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gulanxiu.assets.domain.BuyDevice;
import com.gulanxiu.assets.domain.Device;
import com.gulanxiu.assets.domain.JsonData;
import com.gulanxiu.assets.domain.RepairDevice;
import com.gulanxiu.assets.domain.ScrapDevice;
import com.gulanxiu.assets.domain.TransferDevice;
import com.gulanxiu.assets.domain.json.BuyDeviceForList;
import com.gulanxiu.assets.domain.json.RepairDeviceForList;
import com.gulanxiu.assets.domain.json.RepairExcel;
import com.gulanxiu.assets.domain.json.ScrapDeviceForList;
import com.gulanxiu.assets.domain.json.ScrapForAdd;
import com.gulanxiu.assets.domain.json.TransferDeviceForList;
import com.gulanxiu.assets.domain.json.TransferForAdd;
import com.gulanxiu.assets.service.IDepartmentService;
import com.gulanxiu.assets.service.IDeviceService;
import com.gulanxiu.assets.service.IStudentService;
import com.gulanxiu.assets.utils.ExcelUtils;

import ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.spring.web.json.Json;

@RestController
@RequestMapping("/v1/api/device")
@Api(description = "设备操作")
public class DeviceController
{
	Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private IDeviceService deviceService;

	@Autowired
	private IDepartmentService departmentService;

	@Autowired
	private IStudentService studentService;

	@ApiOperation("设备列表")
	@GetMapping("list_device")
	public JsonData getDeviceListByKey(@RequestParam(value = "page_num", defaultValue = "1") int pageNumer,
			@RequestParam(value = "record", defaultValue = "10") int record, @RequestParam(value = "key",required =false ) String key)
	{

		PageHelper.startPage(pageNumer, record);
		PageInfo<Device> pageInfo = new PageInfo<>(deviceService.getAllDevice(key));
		Map<String, Object> map = new HashMap<>();
		map.put("pages", pageInfo.getPages());
		map.put("page_num", pageNumer);
		map.put("data", pageInfo.getList());
		return JsonData.success(map);
	}

	@ApiOperation("从excel导入数据到数据库")
	@PostMapping("excel_add")
	public JsonData excelAdd(@RequestParam(value = "excelfile") MultipartFile excelfile)
	{
		List<Device> list = ExcelUtils.getDeviceByExcel(excelfile);
		for (Device device : list)
		{
			if (device.getDepartment() != null)
			{
				device.getDepartment()
						.setDepartmentId(departmentService.getIdByName(device.getDepartment().getDepartmentName()));
			}

			if (device.getOwner() != null)
			{
				device.getOwner()
						.setStudentId(studentService.getByNumber(device.getOwner().getStudentNumber()).getStudentId());
			}

		}
		return JsonData.success(deviceService.insertAll(list));
	}

	@ApiOperation("查询某个具体的设备信息")
	@GetMapping("select_one")
	public JsonData selectOne(@RequestParam(value = "device_id") int deviceId)
	{

		try
		{
			return JsonData.success(deviceService.selectOne(deviceId));
		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}
	}

	@ApiOperation("更新某个设备信息")
	@PutMapping("update")
	public JsonData update(@RequestBody Device device)
	{
		return JsonData.success(deviceService.update(device));
	}

	@ApiOperation("增加一个设备")
	@PostMapping("add_device")
	public JsonData addDevice(@RequestBody Device device)
	{
		return JsonData.success(deviceService.addDevice(device));
	}

	@ApiOperation("根据id删除一个设备")
	@PutMapping("delete_device_by_id")
	public JsonData deleteDeviceById(@RequestParam("device_id") int id)
	{
		return JsonData.success(deviceService.deleteDeviceById(id));
	}

	@ApiOperation("导出excel模板")
	@GetMapping("get_template")
	public JsonData getTemplate(HttpServletResponse response)
	{
		ExcelUtils.creatDeviceImportTemplate(response);
		return JsonData.success();
	}

	@ApiOperation("查询设备申购列表")
	@GetMapping("get_buy_device")
	public JsonData getBuyDevice(@RequestParam(value = "page_num", defaultValue = "1") int pageNum,
			@RequestParam(value = "record", defaultValue = "10") int record)
	{
		try
		{
			Map<String, Object> result = new HashMap<>();
			PageHelper.startPage(pageNum, record);
			PageInfo<BuyDeviceForList> pageInfo = new PageInfo<>(deviceService.getBuyDevice());
			result.put("data", pageInfo.getList());
			result.put("page_num", pageNum);
			result.put("pages", pageInfo.getPages());

			return JsonData.success(result);

		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}

	}

	@ApiOperation("增加若干个申购")
	@PostMapping("add_buy_deivces")
	public JsonData addBuyDevices(@RequestBody List<BuyDevice> list)
	{
		try
		{
			return JsonData.success(deviceService.addBuyDevice(list));
		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}
	}

	@ApiOperation("查询设备划拨列表")
	@GetMapping("get_transfer_device")
	public JsonData getTransferDevice(@RequestParam(value = "page_num", defaultValue = "1") int pageNum,
			@RequestParam(value = "record", defaultValue = "10") int record)
	{
		try
		{
			Map<String, Object> result = new HashMap<>();
			PageHelper.startPage(pageNum, record);
			PageInfo<TransferDeviceForList> pageInfo = new PageInfo<>(deviceService.getTransferDevice());
			result.put("data", pageInfo.getList());
			result.put("page_num", pageNum);
			result.put("pages", pageInfo.getPages());

			return JsonData.success(result);

		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}

	}

	@ApiOperation("增加若干个划拨")
	@PostMapping("add_transfer_deivces")
	public JsonData addTransferDevices(@RequestBody TransferForAdd transfer)
	{
		try
		{
		     return JsonData.success(deviceService.addTransferDevices(transfer));
		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}
	}
	
	@ApiOperation("导出划拨excel")
	@GetMapping("get_transfer_excel")
	public JsonData getTransferExcel(@RequestParam("transfer_id") int id , HttpServletResponse response)
	{
		try
		{
			ExcelUtils.TransferDevice(deviceService.getTransferExcel(id), response);
		     return JsonData.success();
		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}
	}
	
	
	

	@ApiOperation("查询设备维修列表")
	@GetMapping("get_repair_device")
	public JsonData getRepairDevice(@RequestParam(value = "page_num", defaultValue = "1") int pageNum,
			@RequestParam(value = "record", defaultValue = "10") int record)
	{
		try
		{
			Map<String, Object> result = new HashMap<>();
			PageHelper.startPage(pageNum, record);
			PageInfo<RepairDeviceForList> pageInfo = new PageInfo<>(deviceService.getRepairDevices());
			result.put("data", pageInfo.getList());
			result.put("page_num", pageNum);
			result.put("pages", pageInfo.getPages());

			return JsonData.success(result);

		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}

	}

	@ApiOperation("增加若干个维修")
	@PostMapping("add_repair_deivces")
	public JsonData addRepairDevices(@RequestBody RepairDevice repairDevice)
	{
		try
		{
			return JsonData.success(deviceService.addRepairDevices(repairDevice));
		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}
	}
	
	@ApiOperation("导出维修Excel")
	@GetMapping("get_repair_excel")
	public JsonData getRepairExcel(@RequestParam("repair_id") int id,HttpServletResponse response)
	{
		try
		{
			RepairExcel rd = deviceService.getRepairExcel(id);
			ExcelUtils.GetRepairExcel(rd,response);
			return JsonData.success(deviceService.getRepairExcel(id));
		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}
	}

	
	@ApiOperation("查询设备废弃列表")
	@GetMapping("get_scrap_device")
	public JsonData getScrapDevice(@RequestParam(value = "page_num", defaultValue = "1") int pageNum,
			@RequestParam(value = "record", defaultValue = "10") int record)
	{
		try
		{
			Map<String, Object> result = new HashMap<>();
			PageHelper.startPage(pageNum, record);
			PageInfo<ScrapDeviceForList> pageInfo = new PageInfo<>(deviceService.getScrapDevices());
			result.put("data", pageInfo.getList());
			result.put("page_num", pageNum);
			result.put("pages", pageInfo.getPages());

			return JsonData.success(result);

		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}

	}

	@ApiOperation("增加若干个废弃")
	@PostMapping("add_scrap_deivces")
	public JsonData addScrapDevices(@RequestBody ScrapForAdd scrap)
	{
		try
		{
			return JsonData.success(deviceService.addScrapDevices(scrap));
		} catch (Exception e)
		{
			logger.error(e.getMessage());
			e.printStackTrace();
			return JsonData.fail(e.getMessage());
		}
	}
	
	@ApiOperation("根据id查询维修的详细信息")
	@GetMapping("get_repair_by_id")
	public JsonData getRepairById(@RequestParam("repair_id") int id)
	{
		try
		{
			return JsonData.success(deviceService.getRepairById(id));
		} catch (Exception e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
		}
	}
	

}
