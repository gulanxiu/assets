package com.gulanxiu.assets.controller;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.util.concurrent.Service;
import com.gulanxiu.assets.domain.BorrowForExcel;
import com.gulanxiu.assets.domain.JsonData;
import com.gulanxiu.assets.domain.json.BorrowForAdd;
import com.gulanxiu.assets.domain.json.BorrowForList;
import com.gulanxiu.assets.domain.json.BorrowForUpdate;
import com.gulanxiu.assets.service.IBorrowService;
import com.gulanxiu.assets.service.IDeviceService;
import com.gulanxiu.assets.utils.ExcelUtils;
import com.gulanxiu.assets.utils.ITemplate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.spring.web.json.Json;

@RestController
@Api(description = "借用操作")
@RequestMapping("/v1/api/borrow")
public class BorrowController
{
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private IBorrowService borrowService;
	
	@Autowired
	private IDeviceService deviceService;
	
	@ApiOperation("显示某人的借用记录状态")
	@GetMapping("show")
	public JsonData getBorrowListByStudentId(@RequestParam(value = "page_num", defaultValue = "1") int pageNumber,
			@RequestParam(value = "record", defaultValue = "10") int record, @RequestParam("student_id") int studentId)
	{
		return JsonData.success(borrowService.getBorrowListByStudentId(studentId, pageNumber, record));
	}

	@ApiOperation("更新借用的状态")
	@PutMapping("update_borrow")
	public JsonData update(@RequestBody BorrowForUpdate borrow)
	{
	
		Integer state = borrowService.getState(borrow.getBorrowId());
		if(state == null)
			return JsonData.fail("没有这条借用");
		if (borrow.getState() == 0 ||(state + 1) % 4 == borrow.getState()  )
		{
			return JsonData.success(borrowService.update(borrow));

		}
		String msg = "你已申请或同意或非法操作";
		logger.error(msg);
		return JsonData.fail(msg);
		
		
		
		
	}
	
	@ApiOperation("查询自己的借用历史")
	@GetMapping("get_history")
	public JsonData getHistory(@RequestParam("student_id") int id,@RequestParam("page")int page,@RequestParam("record")int record)

	{
		return pageTemplate(page, record, () -> {
			return borrowService.getHistory(id);
		});
	}

	@ApiOperation("添加借用")
	@PostMapping("add_borrow")
	public JsonData addBorrow(@RequestBody BorrowForAdd borrow)
	{
		if (deviceService.isExist(borrow.getDeviceId()))
		{
			return JsonData.success(borrowService.addBorrow(borrow));
		}

		else
		{
			String msg = "该设备已被借用或不存在";
			logger.error(msg);
			return JsonData.fail(msg);
		}
	}

	@ApiOperation("按状态查询借用")
	@GetMapping("get_by_state")
	public JsonData getByState(@RequestParam("state") int state,
			@RequestParam(value = "page_num", defaultValue = "1") int pageNum,
			@RequestParam(value = "record", defaultValue = "10") int record)
	{
		try
		{
			HashMap<String, Object> result = new HashMap<>();
			PageHelper.startPage(pageNum, record);
			PageInfo<BorrowForList> pages = new PageInfo<>(borrowService.getByState(state));
			result.put("page_num", pageNum);
			result.put("pages", pages.getPages());
			result.put("data", pages.getList());
			return JsonData.success(result);

		} catch (Exception e)
		{
			logger.error("state非法. 详细:" + e.getMessage());
			return JsonData.fail(e.getMessage());
		}
	}
	
	@ApiOperation("导出请求借用清单")
	@GetMapping("create_borrow_excel")
	public JsonData createBorrowExcel(@RequestParam("borrow_id")int id,HttpServletResponse response)
	{
		return eTemplate(() ->{
			//获取相应的数据
			BorrowForExcel bfe = borrowService.getBorrowForExcel(id);
			ExcelUtils.getBorrowForExcel(bfe, response, bfe.getDeviceName() + "借用表");
			return JsonData.success();
		});
	}

	private  JsonData eTemplate(ExceptionTemplate template)
	{
		try {
			return template.Do();
		}catch(Exception  e)
		{
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
			
		}
	}
	
	//分页

	private JsonData pageTemplate(int page,int record, PageTemplate template)
	{
		PageHelper.startPage(page,record);
		Map<String, Object> result = new HashMap<>();
		PageInfo<BorrowForList> pages = new PageInfo<>(template.func());
		result.put("page_num", page);
		result.put("pages", pages.getPages());
		result.put("data", pages.getList());
		return JsonData.success(result);
		
	}

}

interface PageTemplate
{
	List<BorrowForList> func();

}

interface ExceptionTemplate
{
	JsonData  Do() throws Exception;
	

}
