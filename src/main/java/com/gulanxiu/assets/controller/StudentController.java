package com.gulanxiu.assets.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gulanxiu.assets.domain.JsonData;
import com.gulanxiu.assets.domain.Student;
import com.gulanxiu.assets.service.IStudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@RestController
@Api(description = "学生操作")
@RequestMapping("/v1/api/student")
public class StudentController
{
	
	@Autowired
	private IStudentService studentService;
	@ApiOperation("显示所有学生")
	@GetMapping("show_all_student")
	public JsonData showAllClazz(@RequestParam("page_num") int pageNum,@RequestParam("record")int
			record,@RequestParam(value="state" , defaultValue = "0")int state)
	{
		PageHelper.startPage(pageNum,record);
		PageInfo<Student> pageInfo = new PageInfo<>(studentService.getAll(state));
		Map<String, Object> map = new HashMap<>();
		map.put("data", pageInfo.getList());
		map.put("pages", pageInfo.getPages());
		map.put("page_num", pageNum);
		return JsonData.success(map);
	}
	
	@ApiOperation("根据学号搜索")
	@GetMapping("select_by_studentnumber")
	public JsonData selectByNumber(@RequestParam("student_number")String number)
	{
		return JsonData.success(studentService.selectByStudentNumber(number));
	}
	
	
	@ApiOperation("增加一个学生")
	@PostMapping("add_student")
	public JsonData addClazz(@RequestBody Student student)
	{
		return JsonData.success(studentService.addStudent(student));
	}
	
	@ApiOperation("修改学生信息")
	@PutMapping("update")
	public JsonData update(@RequestBody Student student)
	{
		return JsonData.success(studentService.update(student));
	}
	
	@ApiOperation("查询某个学生")
	@GetMapping("select_one")
	public JsonData selectOne(@RequestParam("student_id") int studentId)
	{
		return JsonData.success(studentService.selectOne(studentId));
	}

	@ApiOperation("删除某个学生")
	@PutMapping("delete_by_id")
	public JsonData deleteById(@RequestParam("student_id")int id)
	{
		return JsonData.success(studentService.deleteById(id));
	}
	
	@ApiOperation("按学号搜索")
	@GetMapping("get_by_number")
	public JsonData getByNumber(@RequestParam("student_number") String number)
	{
		return JsonData.success(studentService.getByNumber(number));
	}

}
