package com.gulanxiu.assets.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gulanxiu.assets.domain.Clazz;
import com.gulanxiu.assets.domain.JsonData;
import com.gulanxiu.assets.service.IClazzService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(description = "班级操作")
public class ClazzController
{
	@Autowired
	private IClazzService clazzService;
	@ApiOperation("显示所有班级")
	@GetMapping("show_all_clazz")
	public JsonData showAllClazz()
	{
		return JsonData.success();
	}
	
	@ApiOperation("根据院系id获取班级信息")
	@GetMapping("get_class_by_department")
	public JsonData getClazzByDeparmtent(@RequestParam("department_id") int id)
	{
		return JsonData.success(clazzService.getClazzByDepartment(id));
	}
	
	
	@ApiOperation("增加一个班级")
	@PostMapping("add_Clazz")
	public JsonData addClazz(@RequestBody Clazz clazz)
	{
		return JsonData.success();
	}
	
	@ApiOperation("修改班级信息")
	@PutMapping("update")
	public JsonData update(@RequestBody Clazz clazz)
	{
		return JsonData.success();
	}
	
	@ApiOperation("查询某个班级")
	@GetMapping("select_one")
	public JsonData selectOne(@RequestParam("clazz_id") int clazz_id)
	{
		return JsonData.success();
	}
}
