package com.gulanxiu.assets.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.gulanxiu.assets.domain.Department;
import com.gulanxiu.assets.domain.JsonData;
import com.gulanxiu.assets.service.IDepartmentService;
import com.gulanxiu.assets.service.impl.DepartmentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(description = "院系操作")
@RequestMapping("/v1/api/department")
public class DepartmentController
{
	@Autowired
	private IDepartmentService departmentService;
	@ApiOperation("显示所有院校")
	@GetMapping("show_all_department")
	public JsonData showAllClazz()
	{
		return JsonData.success(departmentService.getAllDepartment());
	}
	
	
	@ApiOperation("增加一个院校")
	@PostMapping("add_department")
	public JsonData addClazz(@RequestBody Department department)
	{
		return JsonData.success();
	}
	
	@ApiOperation("修改院校信息")
	@PutMapping("update")
	public JsonData update(@RequestBody Department department)
	{
		return JsonData.success();
	}
	
	@ApiOperation("查询某个院校")
	@GetMapping("select_one")
	public JsonData selectOne(@RequestParam("clazz_id") int departmentId)
	{
		return JsonData.success();
	}

}
