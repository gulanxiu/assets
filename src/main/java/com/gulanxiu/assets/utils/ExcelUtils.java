package com.gulanxiu.assets.utils;
import static org.mockito.Mockito.ignoreStubs;

import java.awt.print.Book;
import java.io.File;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.RowBounds;
import org.springframework.web.multipart.MultipartFile;
import com.gulanxiu.assets.domain.BorrowForExcel;
import com.gulanxiu.assets.domain.Department;
import com.gulanxiu.assets.domain.Device;
import com.gulanxiu.assets.domain.RepairDevice;
import com.gulanxiu.assets.domain.Student;
import com.gulanxiu.assets.domain.json.RepairExcel;
import com.gulanxiu.assets.domain.json.TransferForExcel;

import jxl.CellView;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
public class ExcelUtils
{
	private static String SAVE = "D:/data/Java/assets";
	
	
	private static WritableCellFormat getContent() throws Exception
	{
		WritableFont contentfont = new WritableFont(WritableFont.ARIAL, 8);
		WritableCellFormat dataNochu = new WritableCellFormat(contentfont);
		dataNochu.setAlignment(Alignment.getAlignment(2));
		dataNochu.setVerticalAlignment(VerticalAlignment.getAlignment(1));
		dataNochu.setWrap(true);
		return dataNochu;
	}
	
	
	private static WritableCellFormat getTitle() throws Exception
	{
		WritableFont titleFont = new WritableFont(WritableFont.ARIAL, 10);
		WritableCellFormat title = new WritableCellFormat(titleFont);
		title.setAlignment(Alignment.getAlignment(2));
		title.setVerticalAlignment(VerticalAlignment.getAlignment(1));
		return  title;
	}

	public static List<Device> getDeviceByExcel(MultipartFile file)
	{
		List<Device> list = new ArrayList<>();
		try
		{
			Workbook workbook = Workbook.getWorkbook(file.getInputStream());
			Sheet sheet = workbook.getSheet(0);
			int col = 0, row = sheet.getRows();
			String str;
			Device device;
			Department department;
			Student student;
			for (int i = 4; i < row; i++)
			{
				int j = 0;
				device = new Device();
				department = new Department();
				student = new Student();
				str = sheet.getCell(j++, i).getContents();
				department.setDepartmentName(str);
				device.setDepartment(department);
				str = sheet.getCell(j++, i).getContents();
				device.setDeviceNumber(str);
				str = sheet.getCell(j++, i).getContents();
				device.setDeviceName(str);
				str = sheet.getCell(j++, i).getContents();
				device.setDeviceModel(str);
				str = sheet.getCell(j++, i).getContents();
				device.setDeviceLocation(str);
				str = sheet.getCell(j++, i).getContents();
				device.setDevicePrice(new BigDecimal(str));
				str = sheet.getCell(j++, i).getContents();
				device.setDeviceUseDirectory(str);
				str = sheet.getCell(j++, i).getContents();
				device.setDeviceWarranty(Integer.valueOf(str));
				str = sheet.getCell(j++, i).getContents();
				student.setStudentNumber(str);
				device.setOwner(student);
				str = sheet.getCell(j++, i).getContents();
				device.setDeviceFactory(str);
				str = sheet.getCell(j, i).getContents();
				device.setDeviceFactoryNumber(str);
				list.add(device);
			}
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public static void creatDeviceImportTemplate(HttpServletResponse response)
	{
		try
		{
			response.addHeader("Content-Disposition",
					"attachment; filename=" + URLEncoder.encode("设备导入模板.xls", "UTF-8"));
			WritableWorkbook book = Workbook.createWorkbook(response.getOutputStream());

			WritableSheet sheet = book.createSheet("设备登记表", 0);
			int col = 0, row = 0;
			sheet.addCell(new Label(col, row, "设备登记表", getTitle()));
			sheet.mergeCells(col, row, col + 10, row + 2);
			row += 3;
			sheet.addCell(new Label(col, row, "设备所属院系", getContent()));
			col++;
			sheet.addCell(new Label(col, row, "设备编号", getContent()));
			col++;
			sheet.addCell(new Label(col, row, "设备名字", getContent()));
			col++;
			sheet.addCell(new Label(col, row, "设备型号", getContent()));
			col++;
			sheet.addCell(new Label(col, row, "设备存在位置", getContent()));
			col++;
			sheet.addCell(new Label(col, row, "设备价格", getContent()));
			col++;
			sheet.addCell(new Label(col, row, "设备使用方向",getContent()));
			col++;
			sheet.addCell(new Label(col, row, "保修期", getContent()));
			col++;
			sheet.addCell(new Label(col, row, "负责人工号或学号", getContent()));
			col++;
			sheet.addCell(new Label(col, row, "出厂名称", getContent()));
			col++;
			sheet.addCell(new Label(col, row, "出厂联系电话", getContent()));
			book.write();
			book.close();

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void getBorrowForExcel(BorrowForExcel bfe, HttpServletResponse response, String filename)
			throws Exception
	{
		excelTemplate((book ->
		{
			WritableSheet sheet = book.createSheet("借用登记表", 0);
			int col,row = col = 0;
			sheet.addCell(new Label(col,row,"实验室仪器设备借用表",getTitle()));
			sheet.mergeCells(col, row, col + 11, row += 2);
			row ++;
			sheet.addCell(new Label(col,row,"借用申请人",getContent()));
			sheet.mergeCells(col, row, col+=1, row + 1);
			sheet.addCell(new Label(++col , row , bfe.getBorrowPerson(),getContent()));
			sheet.mergeCells(col, row, col +=1, row + 1);
			sheet.addCell(new Label(++col , row , "院系",getContent()));
			sheet.mergeCells(col, row, col +=1, row + 1);
			sheet.addCell(new Label(++col , row ,bfe.getDepartment(),getContent()));
			sheet.mergeCells(col, row, col +=1, row + 1);
			sheet.addCell(new Label(++col , row , "联系电话",getContent()));
			sheet.mergeCells(col, row, col +=1, row + 1);
			sheet.addCell(new Label(++col , row , bfe.getPhone(),getContent()));
			sheet.mergeCells(col, row, col +=1, row += 1);
			row ++;
			col = 0;
			sheet.addCell(new Label(col,row,"学号",getContent()));
			sheet.mergeCells(col, row, col+=1, row + 1);
			sheet.addCell(new Label(++col , row , bfe.getNumber(),getContent()));
			sheet.mergeCells(col, row, col +=1, row + 1);
			sheet.addCell(new Label(++col , row , "班级",getContent()));
			sheet.mergeCells(col, row, col +=1, row + 1);
			sheet.addCell(new Label(++col , row ,bfe.getClazz(),getContent()));
			sheet.mergeCells(col, row, col +=1, row + 1);
			sheet.addCell(new Label(++col , row , "借用日期",getContent()));
			sheet.mergeCells(col, row, col +=1, row + 1);
			sheet.addCell(new Label(++col , row , new SimpleDateFormat("YYYY年MM月dd日").format(bfe.getBorrowDate()),getContent()));
			sheet.mergeCells(col, row, col +=1, row += 1);
			row ++;
			col = 0;
			
			sheet.addCell(new Label(col,row,"借用仪器：设备品牌/规格/机身编号/数量/配件",getContent()));
			sheet.mergeCells(col, row,col += 1 ,row + 3 );
			WritableCellFormat wFormat = getContent();
			wFormat.setAlignment(Alignment.LEFT);
			sheet.addCell(new Label(++col, row , String.format("仪器名称:%-50s"
					+ "规格:%s\n设备品牌:%-50s数量:%d\n机身编号:%-45s配件:%s", 
					bfe.getDeviceName(),bfe.getDeviceModel(),bfe.getDeviceFactory(),
					bfe.getCount(),bfe.getNumber(),bfe.getAccess()),wFormat));
			sheet.mergeCells(col, row, col + 9 , row += 3);
			col =0;
			row ++;
			sheet.addCell(new Label(col,row, "使用性质",getContent()));
			sheet.mergeCells(col, row, col +=1 , row + 3);
			sheet.addCell(new Label(++col,row,bfe.getBecause(),wFormat));
			sheet.mergeCells(col, row, col + 9, row += 3);
			col = 0 ; 
			row ++;
			sheet.addCell(new Label(col,row,"设备归还情况\n\n(须验收和借用双方本人签字确认归还结果)",getContent()));
			sheet.mergeCells(col, row, col += 1, row + 10);
			sheet.addCell(new Label(++col , row , "归还时间",getContent()));
			sheet.mergeCells(col, row, col += 1, row + 1);
			sheet.addCell(new Label(++col , row ,"  年     月    日    时",getContent()));
			sheet.mergeCells(col, row, col += 2, row + 1);
			sheet.addCell(new Label(++ col , row , "是否按时归还",getContent()));
			sheet.mergeCells(col, row, col += 2, row + 1);
			sheet.addCell(new Label(++ col , row,"是____  否____",getContent()));
			sheet.mergeCells(col, row, col += 1 , row + 1);
			row ++;
			col = 1;
			sheet.addCell(new Label(++col , row , "设备情况是否完好____(打勾)",getContent()));
			sheet.mergeCells(col, row, col += 4, row + 1);
			sheet.addCell(new Label(++col , row ,"归还设备是否情况异常____(打勾)",getContent()));
			sheet.mergeCells(col, row, col += 4, row + 1);
			row ++;
			col = 1;
			
			sheet.addCell(new Label(++col,row, "情况说明:(归还设备情况异常时，须说明情况，可附页)\n\n\n"
					+ "设备归还人签名：                    设备归还人(保管负责人) 签名：\n"
					+ "                                 年           月       日                                                         年           月       日",wFormat));
			sheet.mergeCells(col, row, col + 9, row + 6);
			
			

		}),response,filename);
	}

	public static void excelTemplate(ExcelAction action, HttpServletResponse response, String filename) throws Exception
	{
		response.addHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(filename + ".xls", "UTF-8"));
		WritableWorkbook book = Workbook.createWorkbook(response.getOutputStream());
		action.Action(book);
		book.write();
		book.close();

	}

	public static void testTemplate(ExcelAction action) throws Exception
	{
		File file = new File(SAVE, "a.xls");
		WritableWorkbook book = Workbook.createWorkbook(file);
		action.Action(book);
		book.write();
		book.close();
	}


	public static void TransferDevice(TransferForExcel transfer, HttpServletResponse response) throws Exception
	{
		excelTemplate(book -> {
			WritableSheet sheet = book.createSheet("设备划拨单",0);
			int col, row = col = 0;
			
			sheet.addCell(new Label(col,row,"浙江工商大学仪器调拨单",getTitle()));
			sheet.mergeCells(col, row, col + 6, row);
			row ++ ;
			sheet.addCell(new Label(col ++,row,"调出单位:",getContent()));
			sheet.addCell(new Label(col,row,transfer.getOutDepartment(),getContent()));
			sheet.mergeCells(col, row, col + 1, row);
			col +=3;
			sheet.addCell(new Label(col ++, row , "调入单位:",getContent()));
			sheet.addCell(new Label(col,row,transfer.getInDepartment(),getContent()));
			sheet.mergeCells(col, row, col + 1, row);
			row ++;
			col = 0;
			WritableCellFormat format = getContent();
			format.setAlignment(Alignment.LEFT);
			sheet.addCell(new Label(col,row,"制表日期",getContent()));
			sheet.addCell(new Label(col + 1 , row , new SimpleDateFormat("YYYY年MM月dd日").format(new Date()),format));
			sheet.mergeCells(col + 1, row, col + 6, row);
			row ++;
			format = getContent();
			format.setBorder(Border.ALL,BorderLineStyle.DOUBLE );
			String[] titls = new String[] {"序号","仪器编号","仪器名称","型号规格","单价","厂家","购置日期"};
			for(col = 0 ; col < titls.length ; col ++)
			{
				sheet.addCell(new Label(col,row,titls[col],format));
			}
			row ++;
			for(  int i = 0; i < transfer.getList().size(); i ++ )
			{
				col  = 0;
				sheet.addCell(new Label(col,row,transfer.getList().get(i).getId() + "",format));
				col ++ ;
				sheet.addCell(new Label(col,row,transfer.getList().get(i).getNumber(),format));
				col ++ ;
				sheet.addCell(new Label(col,row,transfer.getList().get(i).getName(),format));
				col ++ ;
				sheet.addCell(new Label(col,row,transfer.getList().get(i).getModel() + "",format));
				col ++ ;
				sheet.addCell(new Label(col,row,transfer.getList().get(i).getPrice() + "",format));
				col ++ ;
				sheet.addCell(new Label(col,row,transfer.getList().get(i).getFactory(),format));
				col ++ ;
				sheet.addCell(new Label(col,row,new SimpleDateFormat("YYYY年MM月dd日").format(transfer.getList().get(i).getCreateTime()),format));
				col ++ ;
				row ++;
				
			}
			
			col = 0 ;
			
			sheet.addCell(new Label(col,row,"合计",format));
			sheet.addCell(new Label(col + 1 , row , transfer.getAllPrice() + "",format));
			sheet.mergeCells(col + 1, row, col + 6, row);
			col = 0;
			row ++;
			sheet.addCell(new Label(col,row,"调拨原因",format));
			sheet.mergeCells(col, row, col, row + 3);
			sheet.addCell(new Label(col + 1 , row , transfer.getBecause(),format));
			sheet.mergeCells(col + 1, row, col + 6, row + 3);
			col = 0;
			row += 4;
			String model = "资产管理员:\n使用人:\n           年      月    日 \n  负责人:\n               年      月    日 ";
			sheet.addCell(new Label(col,row,"调入单位",format));
			sheet.mergeCells(col,row, col, row + 4);
			sheet.addCell(new Label(col + 4 ,row, "调出单位",format));
			sheet.mergeCells(col + 4,row, col + 4, row + 4);
			format = getContent();
			format.setBorder(Border.ALL, BorderLineStyle.DOUBLE);
			format.setAlignment(Alignment.LEFT);
			sheet.addCell(new Label(col + 1, row, model,format));
			sheet.mergeCells(col + 1 , row, col + 3, row + 4);
			sheet.addCell(new Label(col + 5, row, model,format));
			sheet.mergeCells(col + 5 , row, col + 6, row + 4);
		},response,"设备划拨表");
		
	}


	public static void GetRepairExcel(RepairExcel rd, HttpServletResponse response) throws Exception
	{
		testTemplate(book -> {
			WritableSheet sheet = book.createSheet("资产维修表", 0);
			int col , row = col = 0;
			sheet.setColumnView(0, 3);
			sheet.addCell(new Label(col,row, rd.getDeviceName() + "维修表",getTitle()));
			sheet.mergeCells(col, row, col +6, row);
			row ++;
			sheet.addCell(new Label(col,row,"部门公章:",getContent()));
			sheet.mergeCells(col, row, col + 1, row);
			col += 2 ;
			sheet.addCell(new Label(col,row , "",getContent()));
			sheet.mergeCells(col, row, col + 1, row);
			col += 2 ;
			sheet.addCell(new Label(col,row , "填报日期:",getContent()));
			col ++ ;
			sheet.addCell(new Label(col,row ,new SimpleDateFormat("YYYY年MM月dd日").format(new Date()),getContent()));
			sheet.mergeCells(col, row, col + 1, row);
			col += 2 ;
			row ++ ;
			col = 0;
			WritableCellFormat format = getContent();
			format.setBorder(Border.ALL, BorderLineStyle.DOUBLE);
			sheet.addCell(new Label(col,row,"设备基本信息",format));
			sheet.mergeCells(col, row, col, row + 5);
			
		});
		
	}

}

interface ExcelAction
{
	void Action(WritableWorkbook wb) throws Exception;
}
