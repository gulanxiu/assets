package com.gulanxiu.assets.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.pagehelper.PageHelper;
import com.gulanxiu.assets.domain.JsonData;

public interface ITemplate
{
	
	default JsonData pageTemplate(int page,int record,ExceptionEntity template)
	{
		return exceptionTemplate(() -> {
			PageHelper.startPage(page,record);
			return JsonData.success( template.func());
		}
		);
	}

	default JsonData exceptionTemplate(ExceptionEntity template)
	{
		try {
			return template.func();
		}catch(Exception  e)
		{
			Logger logger = LoggerFactory.getLogger(getClass());
			logger.error(e.getMessage());
			return JsonData.fail(e.getMessage());
			
		}
	}
}
