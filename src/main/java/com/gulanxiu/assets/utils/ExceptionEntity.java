package com.gulanxiu.assets.utils;

import com.gulanxiu.assets.domain.JsonData;

public interface ExceptionEntity
{
		JsonData func();
}
