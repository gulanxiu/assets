package com.gulanxiu.assets.provider;

import org.apache.ibatis.annotations.Case;
import org.apache.ibatis.jdbc.SQL;

import com.gulanxiu.assets.domain.Borrow;
import com.gulanxiu.assets.domain.json.BorrowForUpdate;

public class BorrowProvider
{
	public String update(BorrowForUpdate borrow)
	{
		SQL sql = new SQL();
		sql.UPDATE("borrow");
		switch (borrow.getState())
		{
		case 0: 
			if(borrow.getAgreeBack() != null)
			{
				sql.SET("agree_back = #{agreeBack}");
			}
			
			if(borrow.getAgreeBorrow() != null)
				sql.SET("agree_borrow = #{agreeBorrow} ");
			if(borrow.getDeny()!=null)
			{
				sql.SET("deny = #{deny}");
			}
			
			break;
		case 2:
			if(borrow.getAgreeBorrow() != null)
				sql.SET("agree_borrow = #{agreeBorrow} ");
			
			break;
			
		case 3:
			if(borrow.getBackBecause() != null)
			{
				sql.SET("return_because = #{backBecause}");
				sql.SET("end_time = now()");
				sql.SET("is_good = #{isGood}");
			}
			
			break;
			
		

		default:
			break;
		}
		
		
		sql.SET("borrow_status = #{state} ");
		
		
		sql.WHERE("borrow_id = #{borrowId}");
		return sql.toString();
	}

}
