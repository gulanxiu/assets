package com.gulanxiu.assets.provider;

import org.apache.ibatis.jdbc.SQL;

import com.gulanxiu.assets.domain.Student;

public class StudentProvider
{
	public String update(Student student)
	{
		SQL sql = new SQL();
		sql.UPDATE("student");
		if(student.getStudentNumber()!=null)
		{
			sql.SET("student_number = #{studentNumber}");
		}
		if(student.getStudentComment() != null)
			sql.SET("student_comment = #{studentComment}");
		if(student.getClazz() != null)
			sql.SET("class_id = #{clazz.classId}");
		if(student.getStudentName() != null)
			sql.SET("student_name = #{studentName}");
		if(student.getStudentPhone() != null)
			sql.SET("student_phone = #{studentPhone}" );
		sql.WHERE("student_id = #{studentId}");
		return sql.toString();
	}

}
