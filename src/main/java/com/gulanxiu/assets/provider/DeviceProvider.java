package com.gulanxiu.assets.provider;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

import com.github.pagehelper.StringUtil;
import com.gulanxiu.assets.domain.Device;

public class DeviceProvider
{
	public String update(Device device)
	{
		SQL sql = new SQL();
		sql.UPDATE("device");
		if (device.getDepartment() != null)
			sql.SET("department_id = #{department.departmentId}");
		if(device.getDeviceCount() != null)
			sql.SET("device_count = #{deviceCount}");
		if(device.getDeviceFactory() != null)
			sql.SET("device_factory = #{deviceFactory}");
		if(device.getDeviceFactoryNumber() != null)
			sql.SET("device_factory_number = #{deviceFactoryNumber}");
		if(device.getDeviceIsBorrow() != null)
			sql.SET("device_id_borrow = #{deviceIsBorrow}");
		if(device.getDeviceLocation() != null)
			sql.SET("device_location = #{deviceLocation}");
		if(device.getDeviceModel() != null)
			sql.SET("device_model = #{deviceModel}");
		if(device.getDeviceName() != null)
			sql.SET("device_name = #{deviceName}");
		if(device.getDeviceNumber() != null)
			sql.SET("device_number = #{deviceNumber}");
		if(device.getDevicePrice() != null)
			sql.SET("device_price = #{devicePrice}");
		if(device.getDeviceStatusEntity() != null)
			sql.SET("device_status = 1 ");
		if(device.getDeviceUseDirectory() != null)
			sql.SET("device_use_directory = #{deviceUseDirectory}");
		if(device.getDeviceWarranty() != null)
			sql.SET("device_warranty = #{deviceWarranty}");
		if(device.getOwner() != null)
			sql.SET("own_id = #{owner.studentId}");

		sql.WHERE("device_id = #{deviceId}");
		return sql.toString();
	}
	
	
	public  String selectbykey(@Param("key")String key)
	{
		SQL sql = new SQL();
		sql.SELECT("*");
		sql.FROM("device");
		if(StringUtil.isEmpty(key))
		{
			sql.WHERE("cast(device_id,varchar) like #{key} or"
					+ "device_number like #{key} or"
					+ "device_name like #{key} or"
					+ "device_model like #{key} or"
					+ "device_location like #{key} or"
					+ "cast(device_price,varchar) like #{key} or"
					+ "cast(device_count,varchar) like #{key} or"
					+ "cast(device_status,varchar) like #{key} or"
					+ "cast(device_is_borrow,varchar) like #{key} or"
					+ "cast(creat_date,varchar) like #{key} or"
					+ "cast(device_warranty,varchar) like #{key} or"
					+ "cast(own_id,varchar) like #{key} or"
					+ "device_factory like #{key} or"
					+ "device_factory_number like #{key}");
		}
		
		return sql.toString();
	}

}
