package com.gulanxiu.assets.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gulanxiu.assets.domain.Clazz;
import com.gulanxiu.assets.mapper.ClazzMapper;
import com.gulanxiu.assets.service.IClazzService;

@Service
public class ClazzService implements IClazzService
{
	@Autowired
	private ClazzMapper clazzMapper;

	@Override
	public List<Clazz> getClazzByDepartment(int id)
	{
		return clazzMapper.getClazzByDepartment(id);
	}

}
