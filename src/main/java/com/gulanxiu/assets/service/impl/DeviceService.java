package com.gulanxiu.assets.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.gulanxiu.assets.domain.BuyDevice;
import com.gulanxiu.assets.domain.Device;
import com.gulanxiu.assets.domain.RepairDevice;
import com.gulanxiu.assets.domain.json.BuyDeviceForList;
import com.gulanxiu.assets.domain.json.RepairDeviceDetail;
import com.gulanxiu.assets.domain.json.RepairDeviceForList;
import com.gulanxiu.assets.domain.json.RepairExcel;
import com.gulanxiu.assets.domain.json.ScrapDeviceForList;
import com.gulanxiu.assets.domain.json.ScrapForAdd;
import com.gulanxiu.assets.domain.json.TransferDeviceForList;
import com.gulanxiu.assets.domain.json.TransferForAdd;
import com.gulanxiu.assets.domain.json.TransferForExcel;
import com.gulanxiu.assets.mapper.DepartmentMapper;
import com.gulanxiu.assets.mapper.DeviceMapper;
import com.gulanxiu.assets.service.IDeviceService;

@Service
public class DeviceService implements IDeviceService
{
	@Autowired
	private DeviceMapper deviceMapper;
	
	@Autowired
	private DepartmentMapper dpmapper;

	@Override
	public List<Device> getAllDevice(String key)
	{

		return key != null ? deviceMapper.getAllDevice(key) : deviceMapper.getAllDevice1();
	}

	@Override
	public int addDevice(Device device)
	{
		deviceMapper.addDevice(device);
		return device.getDeviceId();
	}

	@Override
	public int update(Device device)
	{
		deviceMapper.update(device);
		return device.getDeviceId();
	}

	@Override
	public int deleteDeviceById(int id)
	{
		deviceMapper.deleteDeviceById(id);
		return id;
	}

	@Override
	@Transactional
	public int insertAll(List<Device> list)
	{
		Integer index;
		for (Device device : list)
		{
			index = deviceMapper.getDeviceByNumber(device.getDeviceNumber());
			if (index != null)
			{
				device.setDeviceId(index);
				deviceMapper.update(device);
			} else
			{
				deviceMapper.addDevice(device);
			}
		}
		return list.size();
	}

	@Override
	public Device selectOne(int deviceId)
	{
		return deviceMapper.selectOne(deviceId);
	}

	@Override
	public boolean isExist(int deviceId)
	{

		return deviceMapper.isExist(deviceId) == 1 ? true : false;
	}

	@Override
	public List<BuyDeviceForList> getBuyDevice()
	{
		return deviceMapper.getBuyDevice();
	}

	@Override
	@Transactional
	public int addBuyDevice(List<BuyDevice> list)
	{
		int i = 0;
		for (BuyDevice buyDevice : list)
		{
			deviceMapper.addBuyDevice(buyDevice);
			i++;
		}
		return i;
	}

	@Override
	public List<TransferDeviceForList> getTransferDevice()
	{
		return deviceMapper.getTransferDevice();
	}

	@Override
	@Transactional(isolation = Isolation.READ_UNCOMMITTED)
	public int addTransferDevices(TransferForAdd transfer)
	{
		deviceMapper.addTransferBecause(transfer);
		
		
		for (Integer deviceId : transfer.getIds())
		{
			
			deviceMapper.addTransferDevice(deviceId,transfer.getId());
			deviceMapper.UpdateDepartment(deviceId, transfer.getInId());
		}
		
		return transfer.getId();
	}
	
	
	@Override
	public TransferForExcel getTransferExcel(int id)
	{
		TransferForExcel excel = deviceMapper.getTransferBecause(id);
		
		excel.setList(deviceMapper.getTransferForDevice(id));
		for (int i = 1 ; i <= excel.getList().size() ; i ++ )
		{
			excel.getList().get(i - 1).setId(i);
		}
		
		excel.setAllPrice(deviceMapper.getSumPrice(id).doubleValue());
		return excel;
	}

	@Override
	public List<RepairDeviceForList> getRepairDevices()
	{

		return deviceMapper.getRepairDevices();
	}

	@Override
	public int addRepairDevices(RepairDevice repairDevice)
	{
			deviceMapper.addRepairDevice(repairDevice);

		return repairDevice.getId();
	}

	@Override
	public List<ScrapDeviceForList> getScrapDevices()
	{
		return deviceMapper.getScrapDevices();
	}

	@Override
	@Transactional(isolation = Isolation.READ_UNCOMMITTED)
	public int addScrapDevices(ScrapForAdd list)
	{
		deviceMapper.addScrapBecause(list);
		

		for (int deviceId : list.getDeviceIds())
		{
			deviceMapper.addScrapDevice(deviceId, list.getBecauseId());
		}

		return list.getDeviceIds().size();
	}

	@Override
	public RepairDeviceDetail getRepairById(int id)
	{
		return deviceMapper.getRepairById(id);
	}

	@Override
	public RepairExcel getRepairExcel(int id)
	{
		return deviceMapper.getRepairExcel(id);
	}

}
