package com.gulanxiu.assets.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gulanxiu.assets.domain.Department;
import com.gulanxiu.assets.mapper.DepartmentMapper;
import com.gulanxiu.assets.service.IDepartmentService;

@Service
public class DepartmentService implements IDepartmentService
{
	@Autowired
	private DepartmentMapper departmentMapper;
	
	public DepartmentService(DepartmentMapper departmentMapper)
	{
				this.departmentMapper = departmentMapper;
	}

	@Override
	public List<Department> getAllDepartment()
	{
		return departmentMapper.getAllDepartment(); 
	}

	@Override
	public Integer getIdByName(String departmentName)
	{
		return departmentMapper.getIdByName(departmentName);
	}

}
