package com.gulanxiu.assets.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gulanxiu.assets.domain.Borrow;
import com.gulanxiu.assets.domain.BorrowForExcel;
import com.gulanxiu.assets.domain.json.BorrowForAdd;
import com.gulanxiu.assets.domain.json.BorrowForList;
import com.gulanxiu.assets.domain.json.BorrowForUpdate;
import com.gulanxiu.assets.mapper.BorrowMapper;
import com.gulanxiu.assets.service.IBorrowService;
@Service
public class BorrowService implements IBorrowService
{
	@Autowired
	private BorrowMapper borrowMapper;

	@Override
	public Map<String,Object> getBorrowListByStudentId(int studentId, int pageNumber, int record)
	{
		PageHelper.startPage(pageNumber, record);
		PageInfo<Borrow> pages = new PageInfo<>(borrowMapper.getBorrowListByStudnetId(studentId));
		Map<String , Object> map = new HashMap<>();
		map.put("current_page", pageNumber);
		map.put("total_pages", pages.getPages());
		map.put("data", pages.getList());
		return map;
	}

	@Override
	public int update(BorrowForUpdate borrow)
	{
		borrowMapper.update(borrow);
		return borrow.getBorrowId();
		
	}

	@Override
	public int addBorrow(BorrowForAdd borrow)
	{
		 borrowMapper.addBorrow(borrow);
		 return borrow.getBorrowId();
	}

	@Override
	public List<BorrowForList> getByState(int state)
	{
		
		return borrowMapper.getByState(state );
	}

	@Override
	public Integer getState(int borrowId)
	{
	
		return borrowMapper.getState(borrowId);
	}

	@Override
	public BorrowForExcel getBorrowForExcel(int id)
	{
		return borrowMapper.getBorrowForExcel(id);
	}

	@Override
	public List<BorrowForList> getHistory(int id)
	{
		return borrowMapper.getHistory(id);
	}

}
