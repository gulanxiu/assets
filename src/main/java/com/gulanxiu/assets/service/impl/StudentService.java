package com.gulanxiu.assets.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gulanxiu.assets.domain.Student;
import com.gulanxiu.assets.mapper.StudentMapper;
import com.gulanxiu.assets.service.IStudentService;

@Service
public class StudentService implements IStudentService
{
	@Autowired
	private StudentMapper studentMapper;

	@Override
	public int addStudent(Student student)
	{
		studentMapper.addStudent(student);
		return student.getStudentId();
	}

	@Override
	public int update(Student student)
	{
		studentMapper.update(student);
		return student.getStudentId();
	}

	@Override
	public Student selectOne(int id)
	{
		
		return studentMapper.getStudentById(id);
	}

	@Override
	public int deleteById(int id)
	{
		studentMapper.deleteById(id);
		return id;
	}

	@Override
	public Student selectByStudentNumber(String number)
	{
		return studentMapper.selectByStudentNumber(number);
	}

	@Override
	public List<Student> getAll(int state)
	{
		return studentMapper.getAll(state);
	}

	@Override
	public Student getByNumber(String number)
	{
		return studentMapper.getByNumber(number);
	}
	
	

}
