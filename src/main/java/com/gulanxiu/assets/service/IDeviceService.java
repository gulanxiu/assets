package com.gulanxiu.assets.service;

import java.util.List;

import com.gulanxiu.assets.domain.BuyDevice;
import com.gulanxiu.assets.domain.Device;
import com.gulanxiu.assets.domain.RepairDevice;
import com.gulanxiu.assets.domain.ScrapDevice;
import com.gulanxiu.assets.domain.TransferDevice;
import com.gulanxiu.assets.domain.json.BuyDeviceForList;
import com.gulanxiu.assets.domain.json.RepairDeviceDetail;
import com.gulanxiu.assets.domain.json.RepairDeviceForList;
import com.gulanxiu.assets.domain.json.RepairExcel;
import com.gulanxiu.assets.domain.json.ScrapDeviceForList;
import com.gulanxiu.assets.domain.json.ScrapForAdd;
import com.gulanxiu.assets.domain.json.TransferDeviceForList;
import com.gulanxiu.assets.domain.json.TransferForAdd;
import com.gulanxiu.assets.domain.json.TransferForExcel;

public interface IDeviceService
{
	List<Device> getAllDevice(String key);

	int addDevice(Device device);

	int update(Device device);

	int deleteDeviceById(int id);

	int insertAll(List<Device> list);

	Device selectOne(int deviceId);

	boolean isExist(int deviceId);

	List<BuyDeviceForList> getBuyDevice();

	int addBuyDevice(List<BuyDevice> list);

	List<TransferDeviceForList> getTransferDevice();

	int addTransferDevices(TransferForAdd list);
	
	TransferForExcel getTransferExcel(int id);

	List<RepairDeviceForList> getRepairDevices();

	int addRepairDevices(RepairDevice repairDevice);

	List<ScrapDeviceForList> getScrapDevices();

	int addScrapDevices(ScrapForAdd list);

	RepairDeviceDetail getRepairById(int id);

	RepairExcel getRepairExcel(int id);

}
