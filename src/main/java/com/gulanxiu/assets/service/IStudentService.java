package com.gulanxiu.assets.service;

import java.util.List;

import com.gulanxiu.assets.domain.Student;

public interface IStudentService
{
	int addStudent(Student student);
	
	int update(Student student);
	
	Student selectOne(int id);
	
	int deleteById(int id);

	Student selectByStudentNumber(String number);

	List<Student> getAll(int state);

	Student getByNumber(String number);

}
