package com.gulanxiu.assets.service;

import java.util.List;

import com.gulanxiu.assets.domain.Clazz;

public interface IClazzService
{
	List<Clazz>  getClazzByDepartment(int id);

}
