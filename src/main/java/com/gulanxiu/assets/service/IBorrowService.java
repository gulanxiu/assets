package com.gulanxiu.assets.service;

import java.util.List;
import java.util.Map;
import com.gulanxiu.assets.domain.BorrowForExcel;
import com.gulanxiu.assets.domain.json.BorrowForAdd;
import com.gulanxiu.assets.domain.json.BorrowForList;
import com.gulanxiu.assets.domain.json.BorrowForUpdate;

public interface IBorrowService
{
	Map<String, Object> getBorrowListByStudentId(int studentId,int pageNumber,int record);
	
	int update(BorrowForUpdate borrow);

	int addBorrow(BorrowForAdd borrow);

	 List<BorrowForList> getByState(int state);

	Integer getState(int borrowId);

	BorrowForExcel getBorrowForExcel(int id);

	List<BorrowForList> getHistory(int id);
}
