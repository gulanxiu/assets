package com.gulanxiu.assets.service;

import java.util.List;

import com.gulanxiu.assets.domain.Department;

public interface IDepartmentService
{
	List<Department> getAllDepartment();

	Integer getIdByName(String departmentName);
}
